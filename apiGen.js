/**
 * 动态获取cpc.json 环境需要node
 * node apiGen.js
 * @date 2018-09-03
 * @author zhangYun
 * @copyRight nalong
 * @version 1.0
 */

const fs = require('fs');
const path = require('path');


'use strict';

const apiPath = path.resolve('./api-json');
const apiGenPath = path.resolve('./src/api');

/**
 * 读取api文件
 * @param {String} filePath
 * @return {Array} filePathArray
 */
const listFiles = (filePath)=> {
  let arr = [];
  if (filePath) {
    let stats = fs.statSync(filePath);
    if (stats.isDirectory()) {
      let fileNames = fs.readdirSync(filePath);
      if (fileNames) {
        fileNames.forEach(fileName => {
          arr = arr.concat(listFiles(path.resolve(filePath, fileName)));
        });
      }
    }
    else if (stats.isFile()) {
      arr.push(filePath);
    }
  }
  return arr;
}

if (fs.existsSync(apiGenPath)) {
  listFiles(apiGenPath).forEach(filePath => {
    fs.unlinkSync(filePath);
  });
}
else {
  fs.mkdirSync(apiGenPath);
}

// json文件解析 ====================================================================


const apiModel =
`
import BaseApi from '../common/BaseApi';

// 基础映射配置
const API_KEY = "$[API_KEY]";

/**
 * $[NAME]
 */
export default class $[NAME] extends BaseApi {

$[APIS]

}`;

const methodModel =
`
  /**
   * $[ANNOTATION]
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  $[METHOD_NAME](params, config) {
    return this.requestApi("$[URL]", params, API_KEY, config);
  }
`



const jsonFilePaths = listFiles(apiPath);

jsonFilePaths.forEach(filePath => {

  const fileContent = fs.readFileSync(filePath, {
    encoding: 'utf8'
  });
  const fileName = filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.json'));
  const fileNameCap = fileName.substring(0, 1).toLocaleUpperCase() + fileName.substring(1);
  let model = apiModel;

  let apis = '';
  const apiConfig = JSON.parse(fileContent);
  apiConfig.protocol.forEach(api => {
    let methodName = api.url.replace(/\//g, '');
    methodName = methodName.substring(0, 1).toLocaleLowerCase() + methodName.substring(1);
    let method = methodModel;
    method = method.replace(/\$\[ANNOTATION\]/g, api.description.replace(/\s/g, ''))
    .replace(/\$\[URL\]/g, api.url)
    .replace(/\$\[METHOD_NAME\]/g, methodName);

    apis = apis + method;
  });

  model = model.replace(/\$\[API_KEY\]/g, fileName)
  .replace(/\$\[NAME\]/g, fileNameCap)
  .replace(/\$\[APIS\]/g, apis);

  // 生成文件
  const fd = fs.openSync(`${apiGenPath}/${fileNameCap}.js`, 'w');
  fs.writeSync(fd, model, 0);
  console.log(fileNameCap + '.js生成完成！');

});

console.log('api生成完成！');







