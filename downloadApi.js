/**
 * 动态获取cpc.json 环境需要node
 * node downloadApi.js
 * @date 2018-09-03
 * @author zhangYun
 * @copyRight nalong
 * @version 1.0
 */

const http = require('http');
const fs = require('fs');
const path = require('path');


'use strict';

// json文件配置 ====================================================================
const apiConfig =  {
  openApi: 'http://192.168.5.248:30090/OpenAPI',
  preHospitalFirstAidOpenApi: 'http://192.168.5.248:30080/PreHospitalFirstAidOpenAPI', // 院前分诊急救
  // poctOpenAPI: 'http://192.168.5.248:30090/POCTOpenAPI', //快检设备API
  // timeCollectorOpenApi: 'http://192.168.5.248:30090/TimeCollectorOpenApi', // 时间采集器API
  // statisticsOpenAPI: 'http://192.168.5.248:30090/StatisticsOpenAPI', //统计API
  // timeScreenOpenApi: 'http://192.168.5.248:30090/TimeScreenOpenApi', //时间屏API
  // sirOpenAPi: 'http://192.168.5.248:30090/SIROpenAPi', //手术信息API
  preHospitalCareOpenApi: 'http://192.168.5.248:30080/prehospitalcareopenapi',//院前急救API
  // ecgmanagerOpenApi: 'http://192.168.5.248:30090/ecgmanageropenapi', //心电业务API
  // pushOpenApi: 'http://192.168.5.248:30090/PushOpenAPI',//消息推送API
  // hisInterfaceOpenApi: 'http://192.168.5.248:30090/HisInterfaceOpenApi/', // his接口 API
  // awOpenAPI: 'http://192.168.5.248:30090/AWOpenAPI',
}

const apiPath = path.resolve('./api-json');

/**
 * 读取api文件
 * @param {String} filePath
 * @return {Array} filePathArray
 */
const listFiles = (filePath)=> {
  let arr = [];
  if (filePath) {
    let stats = fs.statSync(filePath);
    if (stats.isDirectory()) {
      let fileNames = fs.readdirSync(filePath);
      if (fileNames) {
        fileNames.forEach(fileName => {
          arr = arr.concat(listFiles(path.resolve(filePath, fileName)));
        });
      }
    }
    else if (stats.isFile()) {
      arr.push(filePath);
    }
  }
  return arr;
}

if (fs.existsSync(apiPath)) {
  listFiles(apiPath).forEach(filePath => {
    fs.unlinkSync(filePath);
  });
}
else {
  fs.mkdirSync(apiPath);
}

// json文件下载 ====================================================================
Object.keys(apiConfig).forEach(key => {

  const destpath = path.resolve(`./api-json/${key}.json`);
  const apiurl = `${apiConfig[key]}/DownLoadProtocol/?type=javascript&protocols=`;

  const req = http.request(apiurl, (res) => {
    res.setEncoding('utf8');
    let str = '';
    res.on('data', (chunk) => {
      str = str + chunk;
    });
    res.on('end', () => {
      fs.open(destpath, 'w+', (err, fd) => {
        fs.writeSync(fd, str, 0);
      });
      console.log(`${key}.json文件已生成成功！`);
    });
  });

  req.on('error', (e) => {
    console.error(`${key}.json文件已生成失败: ${e.message}`);
  });

  req.end();
})
