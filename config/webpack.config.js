const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const process = require('process');

const config = {
  entry: {
    index: ['./src/index.js']
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    // filename: '[name].[chunkhash].bundle.js'
    filename: '[name].[hash].bundle.js',
    publicPath: '/',
  },
  optimization: {
    splitChunks: {
      // include all types of chunks
      chunks: 'all',
      automaticNameDelimiter: '-',
    }
  },
  target: 'web',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          cacheDirectory: true,
          plugins: [['transform-runtime'], ["import", { libraryName: "antd-mobile", style: "css" }]],
          presets: ['es2015', 'react', 'stage-0']
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              localIdentName: '[path]_[name]_[md5:hash:base64:5]_[ext]',
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: './config/postcss.config.js'
              }
            }
          },
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              localIdentName: '[path]_[name]_[md5:hash:base64:5]_[ext]',
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: './config/postcss.config.js'
              }
            }
          },
          {
            loader: 'sass-loader',
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: path.resolve(__dirname, '../'),
      verbose: true
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      templateParameters: {
        commitId: process.env.GIT_LAST_COMMIT_ID
      },
      minify: {
        removeComments: true, // 删除注释
        collapseWhitespace: true, // 删除空白
        keepClosingSlash: true, // 保持单身元素上的斜线
        maxLineLength: 1000, // 指定最大行长度。压缩输出将在有效的HTML分割点处被换行符分割
        minifyCSS: true,
        minifyJS: true,
        minifyURLs: true
      },
    }),
    new CopyWebpackPlugin([
      { from: 'public', to: './' },
    ]),
    new UglifyJsPlugin({
      test: /\.js($|\?)/i,
      sourceMap: true,
      uglifyOptions: {
        ie8: true,
        ecma: 5,
      }
    }),
  ]
};

module.exports = config;