const webpack = require('webpack');
const debug = require('debug')('app:compiler:');
const NodeGit = require('nodegit');
const process = require('process');
const path = require('path');

process.env.DEBUG='app:*';
// 开启debug模式 ，开启该模式会打印debug 输出的日志
debug.enabled = true;
// process.env.NODE_ENV='development';

const pathToRepo = path.resolve("./.git");
NodeGit.Repository.open(pathToRepo)
  .then((repository) => {
    return repository.getHeadCommit();
  })
  .then((commit) => {
    // 最后一次提交的id
    process.env.GIT_LAST_COMMIT_ID = commit.id().tostrS();

    const webpackConfig = require('../config/webpack.config');

    // compile
    const webpackCompiler = webpack(webpackConfig);

    // 开始编译
    debug('webpack 开始构建 ...');
    webpackCompiler.run((err, stats) => {

      const info = stats.toJson();
      if (info.warnings) {
        debug('%s', info.warnings);
      }

      if (err || stats.hasErrors()) {
        debug('webpack 构建失败');
        debug(err || info.errors);
      } else {
        debug('webpack 构建成功');
      }
    });
  });
