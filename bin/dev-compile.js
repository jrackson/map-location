// import compiler from 'compiler';
const debug = require('debug')('app:compiler:');
const path = require('path');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const NodeGit = require('nodegit');

process.env.DEBUG='app:*';
// 开启debug模式 ，开启该模式会打印debug 输出的日志
debug.enabled = true;
// process.env.NODE_ENV='development';
const pathToRepo = path.resolve("./.git");
NodeGit.Repository.open(pathToRepo)
  .then((repository) => {
    return repository.getHeadCommit();
  })
  .then((commit) => {
    // 最后一次提交的id
    process.env.GIT_LAST_COMMIT_ID = commit.id().tostrS();

    const webpackConfig = require('../config/webpack.config');
    // 开发环境配置
    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
    webpackConfig.mode = 'development';
    // webpackConfig.devtool = 'inline-source-map';
    const devServer = {
      proxy: {
        '/prehospital/client': {
          target: 'http://localhost:8080',
          pathRewrite: {'^/prehospital/client' : ''}
        },
        '/prehospital/server': {
          target: 'http://localhost:8080',
          pathRewrite: {'^/prehospital/server' : ''}
        }
      },
      host: '0.0.0.0',
      port: 8080,
      index: '/index.html',
      contentBase: path.join(__dirname, 'public'), // boolean | string | array, static file location
      // compress: true, // enable gzip compression
      clientLogLevel: 'info',
      historyApiFallback: true, // true for index.html upon 404, object for multiple paths
      hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
      hotOnly: true,
      https: false, // true for self-signed, object for cert authority
      noInfo: false, // only errors & warns on hot reload
      open: true,
      openPage: '/',
      overlay: true,
      watchContentBase: true,
      stats: {
        colors: true,
      },
      useLocalIp: true,
      socket: 'socket',
      watchOptions: {
        // aggregateTimeout: 300, // 默认值
        ignored: ['dist', 'node_modules'],
      },
    };


    WebpackDevServer.addDevServerEntrypoints(webpackConfig, devServer);

    // compile
    const compiler = webpack(webpackConfig);

    const server = new WebpackDevServer(compiler, devServer);

    // 开始编译
    debug('webpack start build ...');

    server.listen(8080, '0.0.0.0', () => {
      debug('Starting server on http://localhost:8080');
    });

  });

