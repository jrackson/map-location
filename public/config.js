(function() {
  var config = {
    // 生产环境
    production: {
      webSocketServer: 'ws://192.168.5.248:18427/ws',
      webSocketClient: 'ws://192.168.5.248:18427/ws',
      openApi: '/OpenAPI',
      preHospitalFirstAidOpenApi: '/PreHospitalFirstAidOpenAPI',
      preHospitalCareOpenApi: '/prehospitalcareopenapi',
    },
    // 开发环境
    development: {
      webSocketServer: 'ws://192.168.5.248:18427/ws',
      webSocketClient: 'ws://192.168.5.248:18427/ws',
      openApi: 'http://192.168.5.248:30090/OpenAPI',
      preHospitalFirstAidOpenApi: 'http://192.168.5.248:30080/PreHospitalFirstAidOpenAPI',
      preHospitalCareOpenApi: 'http://192.168.5.248:30080/prehospitalcareopenapi/',
    }
  };

  window.getApiConfig = function(env) {
    return config[env];
  };
})();
