import React from 'react';
import Map from './components/Map/Map';
import {
  BrowserRouter as Router,
  Redirect,
  Switch, Route
} from "react-router-dom";

const Routes = [
  { path: '/map', component: Map },
];

const App = () => (
  <Router>
    <Switch>
    <Redirect exact from="/" to={'/console/map'} />
      <Route path="/console">        
        <Switch>
          {
            Routes.map((route, i) => <Route exact key={i} path={'/console' + route.path} component={route.component} />)
          }
        </Switch>
      </Route>
    </Switch>
  </Router>
);

export default App;