import { Toast } from 'antd-mobile'
import PreHospitalFirstAidOpenApi from '../api/PreHospitalFirstAidOpenApi'

const preHospitalFirstAidOpenApi = new PreHospitalFirstAidOpenApi();
/** 微信SDK配置 */
let CONFIG = {
  jsApiList: ['getLocation'], // JS接口列表
  onNotInWx: () => Toast.fail('定位服务不可用，请在"微信"中打开'), // 不在微信中打开页面事件
  onReady: () => console.log('微信配置验证成功'), // 验证成功事件
  onError: () => Toast.fail('微信配置验证失败'), // 验证失败事件
}

export const isInWeixin = () => {
  var ua = navigator.userAgent.toLowerCase();
  if (ua.match(/MicroMessenger/i) == "micromessenger") {
    return true;
  } else {
    return false;
  }
}

export const locationToUrl = location => {
  const { origin, pathname, search } = location
  return `${origin}${pathname}${search}`
}

export const getLocation = () => {
  return new Promise((resolve, reject) => {
    wx.getLocation({
      type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
      success: res => resolve(res),
      fail: err => reject(err),
      cancel: () => reject('授权失败'),
    });
  })
}

/** 初始化JS接口列表 */
export const buildConfig = config => CONFIG = { ...CONFIG, ...config }

/** 初始化sdk配置 */
export const initConfig = ({ appid, timestamp, nonceStr, signature }) => {
  // const { jsApiList, onNotInWx, onReady, onError } = CONFIG;
  // if (!isInWeixin()) { onNotInWx(); return; };
  // wx.config({
  //   debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
  //   appId: appid, // 必填，公众号的唯一标识
  //   timestamp, // 必填，生成签名的时间戳
  //   nonceStr, // 必填，生成签名的随机串
  //   signature,// 必填，签名
  //   jsApiList, // 必填，需要使用的JS接口列表
  // });
  // wx.ready(onReady);
  // wx.error(onError);
}

export const updateConfig = async (url) => {
  // let res = await preHospitalFirstAidOpenApi.baseDataGetWeChatSignature({ url });
  // initConfig(res['Data']);
}
