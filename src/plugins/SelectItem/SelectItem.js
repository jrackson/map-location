import React, { Component } from 'react';
import { List, Icon } from 'antd-mobile';
import PropTypes from 'prop-types';
import './style.scss';
import Option from './Option';

class SelectItem extends Component {

  static propTypes = {
    data: PropTypes.object, // 数据
    multi: PropTypes.bool, // 是否多选
    value: PropTypes.string, // 值
    onChange: PropTypes.func, // 值改变
    label: PropTypes.any, // 左边label
  }

  constructor(props) {
    super(props);
    this.state = {
      active: false,
      options: [],
      checkedNames: '', // 选中的数据，仅在确定点击时更新
    }
  }

  componentDidMount() {
    const {value, data, multi} = this.props;
    this.init(value, data, multi);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.init(nextProps.value, nextProps.data, nextProps.multi);
    }
  }

  /**
   * 初始化数据
   * @param selected 已选中的数据
   * @param data 接口数据
   * @param multi 是否多选
   */
  init(selected, data, multi) {
    let selecteds = [selected];
    if (multi) {
      selecteds = selected.split(',');
    }

    const options = [];
    Object.keys(data).forEach(key => {
      const select = selecteds.indexOf(key) !== -1;
      const name = data[key];
      options.push(new Option(key, name, select));
    });

    this.setState({
      options: options,
      checkedNames: this.getCheckedNames(options)
    });
  }

  /**
   * 获取已选中的name
   */
  getCheckedNames = (options) => {
    let optionArray = this.state.options;
    if (options) {
      optionArray = options;
    }
    const selecteds = [];
    optionArray.forEach(option => {
      if (option.selected === true) {
        selecteds.push(option.name);
      }
    });
    return selecteds.join(',');
  }

  /**
   * 获取已选中的key
   */
  getCheckedKeys = () => {
    const {options} = this.state;
    const selecteds = [];
    options.forEach(option => {
      if (option.selected === true) {
        selecteds.push(option.key);
      }
    });
    return selecteds.join(',');
  }

  /**
   * 选择框被点击
   */
  onClick = () => {
    const {value, data, multi} = this.props;
    this.init(value, data, multi);
    this.setState({
      active: true,
    });
  }

  /**
   * 点击选择去除选择
   */
  onTab = (option, i) => {
    const {options} = this.state;
    const {multi} = this.props;

    if (multi) {
      options[i].selected = !options[i].selected;
    }
    else {
      options.forEach((item, j) => {
        if (j === i) {
          item.selected = !option.selected;
        }
        else {
          item.selected = false;
        }
      });
    }

    this.setState({
      options: [].concat(options),
    });
  }

  /**
   * 提交
   */
  onConfirm = () => {
    const {onChange} = this.props;

    if (onChange) {
      onChange(this.getCheckedKeys());
    }
    this.setState({
      active: false,
      checkedNames: this.getCheckedNames()
    });
  }

  /**
   * 取消
   */
  onCancel = () => {
    this.setState({
      active: false,
    });
  }

  /**
   * 点击遮罩层
   */
  onClickMask = (e) => {
    if (e.target.nodeName.toLocaleUpperCase() === 'DIV'
      && e.target.className.indexOf('plugins-select-container') !== -1) {
      this.onCancel();
    }
  }

  render() {
    const {active, options, checkedNames} = this.state;
    const {label} = this.props;

    return (
      <div>
        <List.Item arrow="horizontal" onClick={this.onClick} extra={checkedNames}>
          {label}
        </List.Item>
        <div className={'plugins-select-container ' + (active ? 'active' : '')} onClick={this.onClickMask}>
          <div className="data-list">
            <ul>
              {
                options.map(
                  (option, i) => <li key={i} className={(option.selected === true) ? "active" : ''}
                    onClick={() => {
                      this.onTab(option, i)
                    }}
                  >
                    <span>{option.name}</span>
                    <Icon type="check" size="xs"/>
                  </li>
                )
              }
            </ul>
          </div>
          <div className="button-list">
            <div className="button cancel" onClick={this.onCancel}><span>取消</span></div>
            <div className="button confirm" onClick={this.onConfirm}><span>确定</span></div>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectItem;
