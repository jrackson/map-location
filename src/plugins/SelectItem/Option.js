/**
 * 选项数据结构
 */
export default class Option {

  key = ''; // key
  name = ''; // 名称
  selected = false; // 是否选中

  constructor(key, name, selected) {
    this.key = key;
    this.name = name;
    this.selected = selected;
  }
}