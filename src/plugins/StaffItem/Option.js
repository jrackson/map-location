/**
 * 选项数据结构
 */
export default class Option {

  key = ''; // key
  name = ''; // 名称
  selected = false; // 是否选中
  edit = false; // 是否编辑状态
  visible = true; // 是否可见

  constructor(key, name, selected, edit, visible) {
    this.key = key;
    this.name = name;
    this.selected = selected;
    this.edit = edit;
    this.visible = visible;
  }
}