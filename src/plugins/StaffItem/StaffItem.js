import React, {Component} from 'react';
import {List, Icon, Toast} from 'antd-mobile';
import PropTypes from 'prop-types';
import OpenApi from '../../api/OpenApi';
import _ from 'lodash';
import './style.scss';
import Option from './Option';

const LABEL_H = 50, SEARCH_H = 60, SELECT_H = 50, BUTTON_H = 60;

const FIELD_API_KEY = {
  "FMCMedicalStaff": "cpc_fmcmedicalstaff", // 首次医疗接触医护人员
  "InitialDiagnosisAndTreatmentDoctorName": "cpc_initdiagtreatmentdoctorname", // 初步诊断处理医生
  "ACSAntiplateletNurse": "cpc_acsantiplateletnurse", // ACS给药者
  "DecideDoctor": "cpc_decidedoctor", // 急诊PCI决定医生
  "PCIMedicalStaff": "cpc_pcimedicalstaff", // 急诊PCI介入人员
  "OperationFiller": "cpc_operationfiller", // 急诊PCI手术记录人
  "OutCallDoctorName": "cpc_outcalldoctorname", // 来院方式出诊医生
  "OutCallNurseName": "cpc_outcallnursename", // 来院方式出诊护士
  "ReceptionDoctorName": "cpc_receptiondoctorname", // 来院方式接诊医生
  "ReceptionNurseName": "cpc_receptionnursename", // 来院方式接诊护士
  "InternalMedicineReceptionDoctorName": "cpc_interdoctorname", // 心内科接诊医生
  "InternalMedicineReceptionNurseName": "cpc_internursename", // 心内科接诊护士
}

class StaffItem extends Component {

  static propTypes = {
    id: PropTypes.string, // 字段的key
    value: PropTypes.string, // 值
    multi: PropTypes.bool, // 是否多选
    onChange: PropTypes.func, // 值改变
    label: PropTypes.any, // 左边label
    maxLength: PropTypes.number, // 最大长度
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.openApi = new OpenApi();
    this.state = {
      options: [], // 选项
      active: false,
      keyword: '', // 搜索关键字
      confirmChecked: value || '', //  确认的checked
    }
  }

  // 数据库查询的字典
  data = {};

  componentDidMount() {
    this.getData();
  }

  componentWillReceiveProps(nextProps) {
    const {multi} = this.props;
    if (nextProps.value !== this.props.value) {
      this.setState({
        confirmChecked: nextProps.value || ''
      });
      this.init(nextProps.value, this.data, multi);
    }
  }

  /**
   * 初始化数据
   * @param selected 已选中的数据
   * @param data 接口数据
   */
  init(selected, data, multi) {

    let names = [];
    if (selected) {
      names = [selected];
      if (multi) {
        names = selected.split(',');
      }
    }

    // 排序 + 去重 + 取值
    const nameKeyMap = {}, options = [];
    ((Object.keys(data) || []).sort()).forEach((key) => {
      const name = data[key];
      if (!nameKeyMap.hasOwnProperty(name)) {
        const isSelect = names.indexOf(name) !== -1;
        const option = new Option(key, name, isSelect, false, true);
        options.push(option);
      }
      nameKeyMap[name] = key;
    });

    // 解析已选中的数据
    names.forEach(name => {
      if (!nameKeyMap.hasOwnProperty(name)) {
        options.push(new Option('', name, true, true, true));
      }
    });

    this.setState({
      options: options
    });
  }

  /**
   * 获取已选中的name
   */
  getCheckedNames = (options) => {
    let optionArray = this.state.options;
    if (options) {
      optionArray = options;
    }
    const selecteds = [];
    optionArray.forEach(option => {
      if (option.selected === true && option.name !== '') {
        selecteds.push(option.name);
      }
    });
    return selecteds.join(',');
  }

  /**
   * 获取数据
   */
  getData = () => {
    const {value, id, multi} = this.props;
    this.openApi.baseDataGetDictionary({
      'dictionaryClass': [FIELD_API_KEY[id]]
    }).then(res => {
      this.data = res['Data'][FIELD_API_KEY[id]];
      this.init(value, this.data, multi);
    });
  }

  /**
   * 选择框被点击
   */
  onClick = () => {
    const {multi, value} = this.props;
    this.setState({
      active: true,
    });
    this.init(value, this.data, multi);
  }

  /**
   * 提交
   */
  onConfirm = () => {
    const {onChange, maxLength} = this.props;
    const checked = this.getCheckedNames();
    if (checked.length > maxLength) {
      Toast.fail(`最大长度不能超过${maxLength},包括逗号`);
      return;
    }

    if (onChange) {
      onChange(checked);
    }
    this.setState({
      active: false,
      confirmChecked: checked
    });
  }

  /**
   * 取消
   */
  onCancel = () => {
    this.setState({
      active: false,
    });
  }

  /**
   * 点击选择去除选择
   */
  onTab = (e, option, i) => {
    if (e.target.nodeName.toLocaleUpperCase() == 'INPUT') {
      return;
    }
    const {options} = this.state;
    options[i].selected = !options[i].selected;
    this.setState({
      options: [].concat(options)
    });
  }

  /**
   * 新增
   */
  onAdd = () => {

    const {options} = this.state;

    // 前面已经有空字符串了
    let flg = false;
    options.some(option => {
      if (option.name === '' && option.edit === true) {
        flg = true;
        return true;
      }
    });
    if (flg) {
      return;
    }

    const newOptions = [].concat(options);
    // 默认选中
    newOptions.push(new Option('', '', true, true, true));

    this.setState({
      options: newOptions
    });
  }

  /**
   * 用户新增的控件值改变
   */
  onAddChange = (value, index) => {
    const {options} = this.state;

    options[index].name = _.trim(value);
    this.setState({
      options: [].concat(options)
    });
  }

  /**
   * 搜索改变
   */
  onSearchChange = (e) => {
    const value = e.target.value;
    if (value !== undefined) {
      this.setState({
        keyword: _.trim(value)
      })
    }
  }

  /**
   * input失去焦点
   */
  onAddBlur = (e, option, i) => {
    const input = e.target;
    // 处理重名
    const {options} = this.state;
    options.some((option, j) => {
      if (i !== j && _.trim(input.value) === option.name) {
        Toast.fail('已有人员，不能重名');
        input.focus();
        return true;
      }
    });
  }

  /**
   * 搜索过滤
   */
  filter = (options, keyword) => {
    options.forEach(option => {
      const nameIsFind = option.name.indexOf(keyword) !== -1;
      let keyIsFind = false;
      if (option.key !== '') {
        keyIsFind = option.key.indexOf(keyword) !== -1;
      }
      option.visible = nameIsFind || keyIsFind;
    });
    return options;
  }

  render() {
    const {active, options, keyword, confirmChecked} = this.state;
    const {label} = this.props;
    const checked = this.getCheckedNames();
    const height = window.innerHeight - LABEL_H - SEARCH_H - SELECT_H - BUTTON_H;

    return (
      <div>
        <List.Item arrow="horizontal" onClick={this.onClick} extra={confirmChecked}>
          {label}
        </List.Item>
        <div className={'plugins-staff-container ' + (active ? 'active' : '')}>
          <div className="label"><span>选择{label}</span></div>
          <div className="search-container">
            <div className="search">
              <input type="text" value={keyword} placeholder="搜索" onChange={this.onSearchChange}/>
              <Icon type="search" size="xs"/>
            </div>
          </div>
          <div className="data-list" style={{height: height}}>
            <ul>
              {
                this.filter(options, keyword).map((option, i) =>
                  <li key={i}
                    className={
                      (option.visible ? '' : 'hidden ')
                      + (option.edit ? 'add-input' : 'data')
                      + (option.selected ? ' active' : '')
                    }
                    onClick={(e) => {
                      this.onTab(e, option, i)
                    }
                  }>
                    {
                      option.edit ?
                        <input type="text" className="plugins-input" value={option.name}
                               onChange={
                                 (e) => {
                                   this.onAddChange(e.target.value, i);
                                 }
                               }
                               onBlur={(e) => {
                                 this.onAddBlur(e, option, i);
                               }}
                        />
                        : <span>{option.name}</span>
                    }
                    <Icon type="check" size="xs"/>
                  </li>
                )
              }
              <li className="add-button" onClick={this.onAdd}>
                <i className="iconfont icon-add"></i>
                <span>点击新增医生</span>
              </li>
            </ul>
          </div>
          <div className="select-name">
            已选择：{checked}
          </div>
          <div className="button-list">
            <div className="button cancel" onClick={this.onCancel}><span>取消</span></div>
            <div className="button confirm" onClick={this.onConfirm}><span>确定</span></div>
          </div>
        </div>
      </div>
    );
  }
}

export default StaffItem;
