import React, {Component} from 'react';
import RadioGroup from './RadioGroup';
import PropTypes from 'prop-types';
import './style.scss';

class RadioItem extends Component {

  static propTypes = {
    data: PropTypes.object, // 数据
    value: PropTypes.any, // 值
    onChange: PropTypes.func, // 值改变
    label: PropTypes.string, // 左边label
    required: PropTypes.bool, // 是否必填
  };

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {
      checked: value || ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        checked: nextProps.value
      });
    }
  }

  /**
   * 提交
   */
  onConfirm = (key, data) => {
    const {onChange, onchange} = this.props;
    if (onChange) {
      onChange(key);
    }
    if (onchange) {
      onchange(key);
    }
    this.setState({
      checked: key,
    });
  };

  render() {
    const {label, data, required} = this.props;
    const {checked} = this.state;
    return (
      <div>
        <div className="plugins-radio-groupitem-container">
          <div className="plugins-radio-groupitem-label">
            {label}
            {required ? <span>*</span> : ''}
          </div>
          <div className="plugins-radio-groupitem">
            <RadioGroup
              data={data}
              value={checked}
              onChange={this.onConfirm}
            />
          </div>

          {/* <div className="plugins-radio-group">
            {
              Object.keys(data || {}).map((key, i) =>
                <div className="plugins-radio-group-item" key={key} onClick={()=>{
                  this.onConfirm(key, data)
                }}>
                  <div className="item-icon">
                    <i className={"iconfont " + (checked == key ? 'icon-radio-active' : 'icon-radio')}
                    ></i>
                  </div>
                  <div className="item-label"><span>{data[key]}</span></div>
                </div>)
            }
          </div> */}
        </div>
      </div>
    );
  }
}

export default RadioItem;
