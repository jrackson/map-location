import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class RadioGroup extends Component {

  static propTypes = {
    data: PropTypes.object, // 数据
    value: PropTypes.any, // 值
    onChange: PropTypes.func, // 值改变
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {
      checked: value || ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        checked: nextProps.value
      });
    }
  }

  /**
   * 提交
   */
  onConfirm = (key) => {
    const {onChange} = this.props;

    if (onChange) {
      onChange(key);
    }
    this.setState({
      checked: key,
    });
  }


  render() {
    const {data} = this.props;
    const {checked} = this.state;

    return (
      <div className="plugins-radio-group-container">
        {
          Object.keys(data || {}).map((key, i) =>
            <div className="plugins-radio-group-item" key={key} onClick={()=>{
              this.onConfirm(key, data)
            }}>
              <div className="item-icon">
                <i className={"iconfont " + (checked == key ? 'icon-radio-active' : 'icon-radio')}
                ></i>
              </div>
              <div className="item-label"><span>{data[key]}</span></div>
            </div>)
        }
      </div>
    );
  }
}

export default RadioGroup;
