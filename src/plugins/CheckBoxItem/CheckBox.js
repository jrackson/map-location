import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

class CheckBox extends Component {

  static propTypes = {
    checkedValue: PropTypes.any, // 选中时key
    label: PropTypes.string, // label
    value: PropTypes.any, // 值
    onChange: PropTypes.func, // 值改变
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {
      checked: value || ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        checked: nextProps.value
      });
    }
  }

  /**
   * 提交
   */
  onConfirm = (checkedValue) => {
    const {onChange} = this.props;
    const {checked} = this.state;
    const unSelectedValue = (typeof checkedValue === 'boolean') ? false : '';
    const value = checked == checkedValue ? unSelectedValue : checkedValue;

    if (onChange) {
      onChange(value);
    }
    this.setState({
      checked: value,
    });
  }

  render() {
    const {checkedValue, label} = this.props;
    const {checked} = this.state;

    return (
      <div className="plugins-checkbox-container" onClick={()=>{
        this.onConfirm(checkedValue)
      }}>
        <div className="item-icon">
          <i className={"iconfont " + (checked == checkedValue ? 'icon-checkbox-active' : 'icon-checkbox')}
          ></i>
        </div>
        <div className="item-label"><span>{label}</span></div>
      </div>
    );
  }
}

export default CheckBox;
