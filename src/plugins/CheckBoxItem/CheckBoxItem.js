import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CheckBoxGroup from './CheckBoxGroup';
import './style.scss';

class CheckBoxItem extends Component {

  static propTypes = {
    data: PropTypes.object, // 数据
    value: PropTypes.any, // 值
    onChange: PropTypes.func, // 值改变
    label: PropTypes.string, // 左边label
    required: PropTypes.bool, // 是否必填
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {
      checked: value || ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        checked: nextProps.value
      });
    }
  }

  /**
   * 提交
   */
  onConfirm = (value) => {
    const {onChange} = this.props;

    if (onChange) {
      onChange(value);
    }
    this.setState({
      checked: value,
    });
  }

  render() {
    const {label, data, required} = this.props;
    const {checked} = this.props;

    return (
      <div>
        <div className="plugins-checkbox-groupitem-container">
          <div className="plugins-checkbox-groupitem-label">
            {label}
            {required ? <span>*</span> : ''}
          </div>
          <div className="plugins-checkbox-groupitem">
            <CheckBoxGroup
              data={data}
              value={checked}
              onChange={this.onConfirm}
            ></CheckBoxGroup>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckBoxItem;
