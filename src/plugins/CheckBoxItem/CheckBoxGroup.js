import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import CheckBox from './CheckBox';
import './style.scss';

class CheckBoxGroup extends Component {

  static propTypes = {
    data: PropTypes.object, // 数据
    value: PropTypes.any, // 值
    onChange: PropTypes.func, // 值改变
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {
      checkeds: this.getSplit(value)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        checkeds: this.getSplit(nextProps.value)
      });
    }
  }

  /**
   * 获取分隔线
   */
  getSplit = (value) => {
    if (value && typeof value === 'string' && _.trim(value) !== '') {
      return _.trim(value).split(',');
    }
    return [];
  }

  /**
   * 提交
   */
   onConfirm = (selecteKey, srcKey, i) => {
    const {onChange} = this.props;

    let checkeds = this.state.checkeds;
    if (selecteKey !== srcKey) { // 取消
      _.remove(checkeds, (val) => {
        return srcKey === val;
      });
    }
    else { // 选中
      checkeds.push(srcKey);
      checkeds = _.uniq(checkeds);
    }

    if (onChange) {
      onChange(checkeds.join(','));
    }
    this.setState({
      checkeds: checkeds,
    });
  }

  render() {
    const {data} = this.props;
    const {checkeds} = this.state;

    return (
      <div className="plugins-checkbox-group-container">
        {
          Object.keys(data || {}).map((key,i) =>
            <CheckBox
              key={key}
              checkedValue={key}
              label={data[key]}
              value={checkeds.indexOf(key) !== -1 ? key : ''}
              onChange={(value) => {
                this.onConfirm(value, key, i)
              }} ></CheckBox>
          )
        }
      </div>
    );
  }
}

export default CheckBoxGroup;
