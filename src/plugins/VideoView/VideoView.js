import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

/**
 * 视频预览
 * @author zhangYun
 * @copyright nalong
 * @date 2018-10-17
 */
class VideoView extends Component {

  static propTypes = {
    src: PropTypes.any, // src
  }

  constructor(props) {
    super(props);
    this.state = {
      visible: false, // 预览中
    }
  }

  openViewer = () => {
    this.setState({
      visible: true,
    })
  }

  closeViewer = () => {
    this.setState({
      visible: false,
    })
  }

  getViewPort () {
    if(document.compatMode == "BackCompat") {   //浏览器嗅探，混杂模式
      return {
        width: document.body.clientWidth,
        height: document.body.clientHeight
      };
    } else {
      return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight
      };
    }
  }


  render() {
    const config = window.getApiConfig(process.env.NODE_ENV);
    const {thumbSrc, src} = this.props;
    const {visible} = this.state;
    return (
      <div className="plugins-video-view-container">
        <div className="plugins-video-view-thumb"
          onClick={this.openViewer} >
          <img src="/icon_ph/img/icon-player.png"/>
        </div>
        {
          visible ?
          <div className="plugins-video-view-src-container" onClick={this.closeViewer}>
            <div className="video-close" onClick={this.closeViewer}><i className="iconfont icon-close"></i></div>
            <video
              webkit-playsinline="true"
              playsInline="true"
              x-webkit-airplay="allow"
              x5-video-player-type="h5"
              x5-video-player-fullscreen="true"
              x5-video-orientation="portraint"
              className="plugins-video-view-src" src={config.preHospitalFirstAidOpenApi + src}
               controls="controls">
            </video>
          </div>
          : ''
        }
      </div>
    );
  }
}

export default VideoView;
