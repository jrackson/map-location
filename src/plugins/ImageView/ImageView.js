import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

/**
 * 图片预览
 * @author zhangYun
 * @copyright nalong
 * @date 2018-10-17
 */
class ImageView extends Component {

  static propTypes = {
    thumbSrc: PropTypes.any, // 缩略图
    src: PropTypes.any, // src
    width: PropTypes.any, // 原始宽度
    height: PropTypes.any, // 原始高度
  }

  startTime = 0; // 开始触碰时间
  start = null; // 开始触碰定位
  end = null; // 结束触碰定位
  touchesLength = 0; // 触碰手指个数
  previousTouchPoint = null; // 前一次开始触碰定位
  previousTouchTime = 0; // 前一次开始触碰时间
  distance = 0; // 两手指开始触碰距离

  constructor(props) {
    super(props);
    this.state = {
      visible: false, // 预览中
      swipeScale: this.getSwipeScale(), // 缩放精度
    }
  }

  openViewer = () => {
    this.setState({
      visible: true,
      swipeScale: this.getSwipeScale() // 缩放精度
    })
  }

  closeViewer = () => {
    this.setState({
      visible: false,
    })
  }

  /**
   * 获取缩放精度
   */
  getSwipeScale = (swipeScale) => {
    const {width} = this.props;
    const defaultScale = this.getViewPort().width / width;
    if (swipeScale) {
      if (swipeScale > 2) { // 最大放大两倍
        return 2;
      }
      if (swipeScale <= 2 && swipeScale > defaultScale) { // 最大放大两倍
        return swipeScale;
      }
      if (swipeScale < defaultScale) {
        return defaultScale > 1 ? 1 : defaultScale;
      }
    }
    return defaultScale > 1 ? 1 : defaultScale;
  }

  /**
   * 接触开始事件
   */
  onTouchStart = (evt) => {
    this.startTime = new Date().getTime();
    this.touchesLength = evt.touches.length;
    if (evt.touches.length>1) { // 双手势
      let point1 = evt.touches[0];
      let point2 = evt.touches[1];
      let deltaX = Math.abs(point2.pageX - point1.pageX);
      let deltaY = Math.abs(point2.pageY - point1.pageY);
      this.distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY); // 初始时候的距离，r1

    }
    else {//单手势
      this.start = {
        x: evt.touches[0].pageX,
        y: evt.touches[0].pageY
      };
      this.end = {
        x: evt.touches[0].pageX,
        y: evt.touches[0].pageY
      }
      if (this.previousTouchPoint) { // 上一次的触摸点
        if (Math.abs(this.start.x - this.previousTouchPoint.startX) < 10
          && Math.abs(this.start.y - this.previousTouchPoint.startY) < 10
          && this.startTime - this.previousTouchTime < 300){
            // 触发双击的事件
            this.setState({
              swipeScale: 1
            })
            this.previousTouchTime = 0;
            this.previousTouchPoint = null;
            return;
        }
      }
      this.previousTouchTime = this.startTime;
      this.previousTouchPoint = {
        startX: this.start.x,
        startY: this.start.y
      };
    }
  }

  /**
   * 接触移动事件
   */
  onTouchMove = (evt) =>{
    if (evt.touches.length > 1) {
      let point1 = evt.touches[0];
      let point2 = evt.touches[1];
      let deltaX = Math.abs(point2.pageX - point1.pageX);
      let deltaY = Math.abs(point2.pageY - point1.pageY);
      let distance = Math.sqrt( deltaX * deltaX + deltaY * deltaY);
      if (this.distance){
        const addSwipeScale = distance / this.distance;
        // 执行缩放事件
        this.setState({
          swipeScale: this.getSwipeScale(addSwipeScale)
        })
      }
    }
    else { //单手势事件
      this.end = {
        x: evt.touches[0].pageX,
        y: evt.touches[0].pageY
      }
      //触发移动事件
    }
  }

  /**
   * 接触事件结束
   */
  onTouchEnd = (evt) => {
    if (this.touchesLength > 1) {

    }
    else { // 单手势
      let timer = setTimeout(() => {
        // 没有移动超过10
        if (this.previousTouchPoint !== null
          && this.previousTouchTime !== 0
          && Math.abs(this.start.x - this.end.x) <= 0
          && Math.abs(this.start.y - this.end.y) <= 0){
          // 触发单击事件
          this.closeViewer();
        } else {
          clearTimeout(timer); //去除监听
        }
      }, 300);
    }
  }

  getViewPort () {
    if(document.compatMode == "BackCompat") {   //浏览器嗅探，混杂模式
      return {
        width: document.body.clientWidth,
        height: document.body.clientHeight
      };
    } else {
      return {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight
      };
    }
  }


  render() {
    const config = window.getApiConfig(process.env.NODE_ENV);
    const {thumbSrc, src, width, height} = this.props;
    const {visible, swipeScale} = this.state;
    const viewPort = this.getViewPort();
    const style = {
      // transform: "scale(" + swipeScale + ")",
      width: width * swipeScale,
      height: height * swipeScale,
      marginTop: ((viewPort.height - height * swipeScale) > 0 ?
       ((viewPort.height - height * swipeScale) / 2) : 40) + 'px',
    }
    return (
      <div className="plugins-image-view-container">
        <img className="plugins-image-view-thumb" src={config.preHospitalFirstAidOpenApi + thumbSrc}
          onClick={()=> {
            this.openViewer();
        }}/>
        {
          visible ?
          <div className="plugins-image-view-src-container"
            onTouchStart={this.onTouchStart}
            onTouchMove={this.onTouchMove}
            onTouchEnd={this.onTouchEnd}>
            <img style={style} className="plugins-image-view-src" src={config.preHospitalFirstAidOpenApi + src} />
          </div>
          : ''
        }
      </div>
    );
  }
}

export default ImageView;
