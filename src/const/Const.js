export const STORGE_KEY = {
  IS_LOGIN: 'isLoginPh',
  TOKEN: 'tokenPh',
  USER: 'userInfoPh',
  WS_TOKEN: 'wsTokenPh', // websocket token
  OPEN_ID: 'openIdPh', // 微信浏览器openid
  REDIRECT_OATH_URL: 'redirectOathUrlPh', // 微信浏览器回跳地址
  TAB: 'tabPh'
};

export const RESULT = {
  SUCCESS: 0,
  TOKEN_INVALID: 10,
  TOKEN_MISSING: 12,
  SERVER_BUSY: 1000,
  ILLEGAL_OPERATION: 2000,
  PARAMETER_ERROR: 2001,

  // 访问频率超限 2002
  ILLEGAL_PROTOCOL: 2003,
  ILLEGAL_ID: 2004,
  ILLEGAL_PAGESIZE: 2005,

  // 错误信息放在data里 key
  MSG_DATA_CODE: 3004,
}

// websocket
export const WEB_SOCKET = {
  HEART_BEAT_TIME: 20000, // 心跳包间隔时间/ms
}

export const MSG_TYPE = {
  TEXT: 1, // 1:文本,
  IMAGE: 2, // 2:图片,
  VIDEO: 3, // 3:视频
}

export const MSG_BUSINESS_TYPE = {
  GENERAL_MSG: 0, // 0:普通消息,
  CREATE_PATIENT: 1, // 1:创建急救患者,
  UPDATE_PATIENT: 2, // 2:更新患者资料，
  ACCEPT_PATIENT: 3, // 3:受理病人，
  CLOSE_PATIENT: 4, // 4:结束受理，
  START_CHEST_PAIN: 5, // 5:启动胸痛,
  GRACE_GRADE: 6, // 6:Grace评分,
  VITAL_SIGNS: 7, // 7:生命体征,
  START_STROKE: 8, // 8:启动卒中,
  FAST_GRADE: 9, // 9:Fast评分,
  NIHSS_GRADE: 10, // 10:NIHSS评分,
}

export const EVALUATIONTYPE = {
  FAST: 0,
  NIHSS: 1
}