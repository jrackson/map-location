
import BaseApi from '../common/BaseApi';

// 基础映射配置
const API_KEY = "preHospitalCareOpenApi";

/**
 * PreHospitalCareOpenApi
 */
export default class PreHospitalCareOpenApi extends BaseApi {


  /**
   * 采集端提交报告信息（院前APP采集心电图会调用）
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eCGReportSubmitReport(params, config) {
    return this.requestApi("ECGReport/SubmitReport", params, API_KEY, config);
  }

  /**
   * 获取报告的诊断信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eCGReportGetReportDetail(params, config) {
    return this.requestApi("ECGReport/GetReportDetail", params, API_KEY, config);
  }

  /**
   * 获取报告的阴性和阳性
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  diagnosisSTEMIInfo(params, config) {
    return this.requestApi("Diagnosis/STEMIInfo", params, API_KEY, config);
  }

  /**
   * 保存诊断结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  diagnosisSaveDiagResult(params, config) {
    return this.requestApi("Diagnosis/SaveDiagResult", params, API_KEY, config);
  }

  /**
   * 根据报告Id获取诊断结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  diagnosisGetDiagnosisResultByReportId(params, config) {
    return this.requestApi("Diagnosis/GetDiagnosisResultByReportId", params, API_KEY, config);
  }

  /**
   * 自建任务需要的字典
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyGetEmergenyDictionary(params, config) {
    return this.requestApi("ChestPainEmergency/GetEmergenyDictionary", params, API_KEY, config);
  }

  /**
   * 自建任务
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyCreateChestPainEmergencyByManual(params, config) {
    return this.requestApi("ChestPainEmergency/CreateChestPainEmergencyByManual", params, API_KEY, config);
  }

  /**
   * 查询急诊信息分页列表【后台专用】
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyGetChestPainEmergencyList(params, config) {
    return this.requestApi("ChestPainEmergency/GetChestPainEmergencyList", params, API_KEY, config);
  }

  /**
   * 获取胸痛急救详情
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyGetChestPainEmergencyDetail(params, config) {
    return this.requestApi("ChestPainEmergency/GetChestPainEmergencyDetail", params, API_KEY, config);
  }

  /**
   * 获取急救详情
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyGetEmergencyDetail(params, config) {
    return this.requestApi("ChestPainEmergency/GetEmergencyDetail", params, API_KEY, config);
  }

  /**
   * 获取院前胸痛急救信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyGetChestPainEmergency(params, config) {
    return this.requestApi("ChestPainEmergency/GetChestPainEmergency", params, API_KEY, config);
  }

  /**
   * 更新急救任务信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyUpdateChestPainEmergencyDetail(params, config) {
    return this.requestApi("ChestPainEmergency/UpdateChestPainEmergencyDetail", params, API_KEY, config);
  }

  /**
   * 删除消息守护信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyRemovePushMessageGuard(params, config) {
    return this.requestApi("ChestPainEmergency/RemovePushMessageGuard", params, API_KEY, config);
  }

  /**
   * 定时上报坐标信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  gISRealTimeCoord(params, config) {
    return this.requestApi("GIS/RealTimeCoord", params, API_KEY, config);
  }

  /**
   * 上报坐标信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  gISRealTimeCoords(params, config) {
    return this.requestApi("GIS/RealTimeCoords", params, API_KEY, config);
  }

  /**
   * 获得一次急救的所有坐标信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  gISCoordInfo(params, config) {
    return this.requestApi("GIS/CoordInfo", params, API_KEY, config);
  }

  /**
   * 获取波形信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  reportGetWaveforms(params, config) {
    return this.requestApi("Report/GetWaveforms", params, API_KEY, config);
  }

  /**
   * 获取波形信息(只取波形部分)
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  reportGetWaveform(params, config) {
    return this.requestApi("Report/GetWaveform", params, API_KEY, config);
  }

  /**
   * 设备认证
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  accountDeviceLogin(params, config) {
    return this.requestApi("Account/DeviceLogin", params, API_KEY, config);
  }

  /**
   * 系统登录，返回token
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  accountUserLogin(params, config) {
    return this.requestApi("Account/UserLogin", params, API_KEY, config);
  }

  /**
   * 获取系统菜单
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  accountGetMenu(params, config) {
    return this.requestApi("Account/GetMenu", params, API_KEY, config);
  }

  /**
   * 接受120急救系统的时间节点完成时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  api120ReceiveEventTime(params, config) {
    return this.requestApi("Api120/ReceiveEventTime", params, API_KEY, config);
  }

  /**
   * 接受120系统推送过来的急救信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  api120Receive120SysInfo(params, config) {
    return this.requestApi("Api120/Receive120SysInfo", params, API_KEY, config);
  }

  /**
   * 根据120急救ID获取相应MODEL
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  api120GetOTZEmergencyModel(params, config) {
    return this.requestApi("Api120/GetOTZEmergencyModel", params, API_KEY, config);
  }

  /**
   * 提交快检结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  assayDataAssayDataListSubmit(params, config) {
    return this.requestApi("AssayData/AssayDataListSubmit", params, API_KEY, config);
  }

  /**
   * 获取一次急救的所有检测结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  assayDataAssayDataList(params, config) {
    return this.requestApi("AssayData/AssayDataList", params, API_KEY, config);
  }

  /**
   * 获取一次急救的所有检测结果的分页列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  assayDataPageAssayDataList(params, config) {
    return this.requestApi("AssayData/PageAssayDataList", params, API_KEY, config);
  }

  /**
   * 获取组织机构列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationGetOrganizationList(params, config) {
    return this.requestApi("EmergencyOrganization/GetOrganizationList", params, API_KEY, config);
  }

  /**
   * 获取卒中或胸痛的医院结构列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationGetGreenChannelOrgList(params, config) {
    return this.requestApi("EmergencyOrganization/GetGreenChannelOrgList", params, API_KEY, config);
  }

  /**
   * 获取急救组织机构
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationGetEmergencyOrganizationList(params, config) {
    return this.requestApi("EmergencyOrganization/GetEmergencyOrganizationList", params, API_KEY, config);
  }

  /**
   * 选择前往的急救机构
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationChooseEmergencyOrganization(params, config) {
    return this.requestApi("EmergencyOrganization/ChooseEmergencyOrganization", params, API_KEY, config);
  }

  /**
   * 添加机构急救能力
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationAddOrgAbility(params, config) {
    return this.requestApi("EmergencyOrganization/AddOrgAbility", params, API_KEY, config);
  }

  /**
   * 修改机构急救能力
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationUpdateOrgAbility(params, config) {
    return this.requestApi("EmergencyOrganization/UpdateOrgAbility", params, API_KEY, config);
  }

  /**
   * 删除机构急救能力
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationDeleteOrgAbility(params, config) {
    return this.requestApi("EmergencyOrganization/DeleteOrgAbility", params, API_KEY, config);
  }

  /**
   * 机构急救能力列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationGetOrgAbilitys(params, config) {
    return this.requestApi("EmergencyOrganization/GetOrgAbilitys", params, API_KEY, config);
  }

  /**
   * 根据id获取机构能力详情
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyOrganizationGetOrgAbility(params, config) {
    return this.requestApi("EmergencyOrganization/GetOrgAbility", params, API_KEY, config);
  }

  /**
   * 分类型绑定医护人员
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyStaffBindMedicalStaff(params, config) {
    return this.requestApi("EmergencyStaff/BindMedicalStaff", params, API_KEY, config);
  }

  /**
   * 根据类型获取医护人员列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyStaffGetEmergencyStaffsByType(params, config) {
    return this.requestApi("EmergencyStaff/GetEmergencyStaffsByType", params, API_KEY, config);
  }

  /**
   * 记录节点完成时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeRecordNodeTime(params, config) {
    return this.requestApi("ChestPainEmergencyTime/RecordNodeTime", params, API_KEY, config);
  }

  /**
   * 记录谈话结束的时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeRecordTalkEndTime(params, config) {
    return this.requestApi("ChestPainEmergencyTime/RecordTalkEndTime", params, API_KEY, config);
  }

  /**
   * 记录时间点延迟原因
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeRecordTimeDelayReason(params, config) {
    return this.requestApi("ChestPainEmergencyTime/RecordTimeDelayReason", params, API_KEY, config);
  }

  /**
   * 修改时间及延迟原因
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeUpdateTimeDelayReason(params, config) {
    return this.requestApi("ChestPainEmergencyTime/UpdateTimeDelayReason", params, API_KEY, config);
  }

  /**
   * 获取本次急救的相关时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeGetEmergencyNodeTime(params, config) {
    return this.requestApi("ChestPainEmergencyTime/GetEmergencyNodeTime", params, API_KEY, config);
  }

  /**
   * 获取本次急救的所有时间节点的时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeGetAllEmergencyNodeNodeTime(params, config) {
    return this.requestApi("ChestPainEmergencyTime/GetAllEmergencyNodeNodeTime", params, API_KEY, config);
  }

  /**
   * 时间提交接口
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeSaveEmergencyTime(params, config) {
    return this.requestApi("ChestPainEmergencyTime/SaveEmergencyTime", params, API_KEY, config);
  }

  /**
   * 获取各个时间节点对应的延时原因
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chestPainEmergencyTimeGetNodeDelayReasonList(params, config) {
    return this.requestApi("ChestPainEmergencyTime/GetNodeDelayReasonList", params, API_KEY, config);
  }

  /**
   * 根据评估类型获取评估题目及选项
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationGetEvaluation(params, config) {
    return this.requestApi("Evaluation/GetEvaluation", params, API_KEY, config);
  }

  /**
   * 批量返回评估工具的题目及选项列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationGetEvaluationList(params, config) {
    return this.requestApi("Evaluation/GetEvaluationList", params, API_KEY, config);
  }

  /**
   * 根据急救ID和评分工具编号Id获取评估结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationGetEvaluationResult(params, config) {
    return this.requestApi("Evaluation/GetEvaluationResult", params, API_KEY, config);
  }

  /**
   * 根据急救ID获取评估结果列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationGetEvaluationResultList(params, config) {
    return this.requestApi("Evaluation/GetEvaluationResultList", params, API_KEY, config);
  }

  /**
   * 提交评估结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationSubmitEvaluationResult(params, config) {
    return this.requestApi("Evaluation/SubmitEvaluationResult", params, API_KEY, config);
  }

  /**
   * 获取评估模块
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  evaluationGetEvaluationModuleList(params, config) {
    return this.requestApi("Evaluation/GetEvaluationModuleList", params, API_KEY, config);
  }

  /**
   * 获取患者性别数据集
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetPatientGenderList(params, config) {
    return this.requestApi("EmergencyPatients/GetPatientGenderList", params, API_KEY, config);
  }

  /**
   * 获取年龄单位数据集
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetAgeUnitList(params, config) {
    return this.requestApi("EmergencyPatients/GetAgeUnitList", params, API_KEY, config);
  }

  /**
   * 获取患者发病时间区间数据集
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetPatientAttackTimeSectionList(params, config) {
    return this.requestApi("EmergencyPatients/GetPatientAttackTimeSectionList", params, API_KEY, config);
  }

  /**
   * 获取患者主诉数据集
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetPatientComplainList(params, config) {
    return this.requestApi("EmergencyPatients/GetPatientComplainList", params, API_KEY, config);
  }

  /**
   * 根据胸痛急救ID获取急救患者信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetEmergencyPatientByCppId(params, config) {
    return this.requestApi("EmergencyPatients/GetEmergencyPatientByCppId", params, API_KEY, config);
  }

  /**
   * 获取未知的患者姓名
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientsGetEmergencyPatientName(params, config) {
    return this.requestApi("EmergencyPatients/GetEmergencyPatientName", params, API_KEY, config);
  }

  /**
   * 获取波形信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  testSaveDiagnosisRequest(params, config) {
    return this.requestApi("Test/SaveDiagnosisRequest", params, API_KEY, config);
  }

  /**
   * 获取波形信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  testSaveDiagnosis(params, config) {
    return this.requestApi("Test/SaveDiagnosis", params, API_KEY, config);
  }

  /**
   * 获取溶栓核查问题列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  thrombolyticVerificationGetThrombolyticVerificationList(params, config) {
    return this.requestApi("ThrombolyticVerification/GetThrombolyticVerificationList", params, API_KEY, config);
  }

  /**
   * 提交溶栓核查结果接口
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  thrombolyticVerificationSaveThrombolyticVerificationResult(params, config) {
    return this.requestApi("ThrombolyticVerification/SaveThrombolyticVerificationResult", params, API_KEY, config);
  }

  /**
   * 一次急救的溶栓结果
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  thrombolyticVerificationGetETThrombolyticResults(params, config) {
    return this.requestApi("ThrombolyticVerification/GetETThrombolyticResults", params, API_KEY, config);
  }

  /**
   * 根据回答的问题结果判断是否可以做溶栓
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  thrombolyticVerificationThrombolyticVerificationResult(params, config) {
    return this.requestApi("ThrombolyticVerification/ThrombolyticVerificationResult", params, API_KEY, config);
  }


}