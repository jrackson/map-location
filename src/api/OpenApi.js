
import BaseApi from '../common/BaseApi';

// 基础映射配置
const API_KEY = "openApi";

/**
 * OpenApi
 */
export default class OpenApi extends BaseApi {


  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  doctorGetDoctorPager(params, config) {
    return this.requestApi("Doctor/GetDoctorPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseReportFormPager(params, config) {
    return this.requestApi("CaseReportForm/GetCaseReportFormPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetStrokeCaseReportFormPager(params, config) {
    return this.requestApi("CaseReportForm/GetStrokeCaseReportFormPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetArchiveCaseReportFormPager(params, config) {
    return this.requestApi("CaseReportForm/GetArchiveCaseReportFormPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseReportFormListOfLastTwoDay(params, config) {
    return this.requestApi("CaseReportForm/GetCaseReportFormListOfLastTwoDay", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseReportFormData(params, config) {
    return this.requestApi("CaseReportForm/GetCaseReportFormData", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetObjectCaseReportFormData(params, config) {
    return this.requestApi("CaseReportForm/GetObjectCaseReportFormData", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetObjectCaseReportFormDataByIds(params, config) {
    return this.requestApi("CaseReportForm/GetObjectCaseReportFormDataByIds", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetObjectCaseReportFormDataWithFuzzy(params, config) {
    return this.requestApi("CaseReportForm/GetObjectCaseReportFormDataWithFuzzy", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetObjectCaseReportFormList(params, config) {
    return this.requestApi("CaseReportForm/GetObjectCaseReportFormList", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormAddCaseReportForm(params, config) {
    return this.requestApi("CaseReportForm/AddCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormUpdateCaseReportForm(params, config) {
    return this.requestApi("CaseReportForm/UpdateCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormDeleteCaseReportForm(params, config) {
    return this.requestApi("CaseReportForm/DeleteCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormAuditCaseReportForm(params, config) {
    return this.requestApi("CaseReportForm/AuditCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormArchiveCaseReportForm(params, config) {
    return this.requestApi("CaseReportForm/ArchiveCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormArchiveCaseReportForms(params, config) {
    return this.requestApi("CaseReportForm/ArchiveCaseReportForms", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormFollowUpCaseReportForms(params, config) {
    return this.requestApi("CaseReportForm/FollowUpCaseReportForms", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseReportFormOperationPermissions(params, config) {
    return this.requestApi("CaseReportForm/GetCaseReportFormOperationPermissions", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseReportFormDataOperationLogs(params, config) {
    return this.requestApi("CaseReportForm/GetCaseReportFormDataOperationLogs", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormArchiveFailReason(params, config) {
    return this.requestApi("CaseReportForm/ArchiveFailReason", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetOperationLog(params, config) {
    return this.requestApi("CaseReportForm/GetOperationLog", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetWaitForArchiveCaseReportHospitalization(params, config) {
    return this.requestApi("CaseReportForm/GetWaitForArchiveCaseReportHospitalization", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormUpdateCpcFormListHisDischargedTimeRequest(params, config) {
    return this.requestApi("CaseReportForm/UpdateCpcFormListHisDischargedTimeRequest", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormAddSyncHisDischargedTimeRecord(params, config) {
    return this.requestApi("CaseReportForm/AddSyncHisDischargedTimeRecord", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  caseReportFormGetCaseFormIdByInterfaceRelatedId(params, config) {
    return this.requestApi("CaseReportForm/GetCaseFormIdByInterfaceRelatedId", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentUploadECGFileOfByte(params, config) {
    return this.requestApi("Attachment/UploadECGFileOfByte", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentGetECGFileOfByte(params, config) {
    return this.requestApi("Attachment/GetECGFileOfByte", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentDeleteECGFile(params, config) {
    return this.requestApi("Attachment/DeleteECGFile", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentUploadAttachmentResources(params, config) {
    return this.requestApi("Attachment/UploadAttachmentResources", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentGetAttachmentResources(params, config) {
    return this.requestApi("Attachment/GetAttachmentResources", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentDeleteAttachmentResources(params, config) {
    return this.requestApi("Attachment/DeleteAttachmentResources", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentView(params, config) {
    return this.requestApi("Attachment/View", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentGetQRcode(params, config) {
    return this.requestApi("Attachment/GetQRcode", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataConvertConvertToDicFields(params, config) {
    return this.requestApi("DataConvert/ConvertToDicFields", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationCreateCaseReportForm(params, config) {
    return this.requestApi("DataRelation/CreateCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationUpdateCaseReportFormWithFuzzyMatching(params, config) {
    return this.requestApi("DataRelation/UpdateCaseReportFormWithFuzzyMatching", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationUpdateCaseReportForm(params, config) {
    return this.requestApi("DataRelation/UpdateCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationReverseRelateCaseReportForm(params, config) {
    return this.requestApi("DataRelation/ReverseRelateCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationConvertToCaseReportForm(params, config) {
    return this.requestApi("DataRelation/ConvertToCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationBatchConvertToCaseReportForm(params, config) {
    return this.requestApi("DataRelation/BatchConvertToCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationInterfaceBatchConvertToCaseReportForm(params, config) {
    return this.requestApi("DataRelation/InterfaceBatchConvertToCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataRelationConfirmRelateCaseReportForm(params, config) {
    return this.requestApi("DataRelation/ConfirmRelateCaseReportForm", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eventCreateEvent(params, config) {
    return this.requestApi("Event/CreateEvent", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eventCreateEvents(params, config) {
    return this.requestApi("Event/CreateEvents", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eventGetEvent(params, config) {
    return this.requestApi("Event/GetEvent", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  eventGetEventPager(params, config) {
    return this.requestApi("Event/GetEventPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  hisInterfaceRelatedInfoGetNoRelatedDataList(params, config) {
    return this.requestApi("HisInterfaceRelatedInfo/GetNoRelatedDataList", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  hisInterfaceRelatedInfoGetTemplateInfoByCode(params, config) {
    return this.requestApi("HisInterfaceRelatedInfo/GetTemplateInfoByCode", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  unitTestingHelperGetLastRecordByCRFId(params, config) {
    return this.requestApi("UnitTestingHelper/GetLastRecordByCRFId", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  unitTestingHelperGetMedicineGroup(params, config) {
    return this.requestApi("UnitTestingHelper/GetMedicineGroup", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetDictionary(params, config) {
    return this.requestApi("BaseData/GetDictionary", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetSystemInfo(params, config) {
    return this.requestApi("BaseData/GetSystemInfo", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetHospitalID(params, config) {
    return this.requestApi("BaseData/GetHospitalID", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetCaseReportFormModuleCodes(params, config) {
    return this.requestApi("BaseData/GetCaseReportFormModuleCodes", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetMedicineGroup(params, config) {
    return this.requestApi("BaseData/GetMedicineGroup", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetQrCodeConfig(params, config) {
    return this.requestApi("BaseData/GetQrCodeConfig", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetModuleGroupConfig(params, config) {
    return this.requestApi("BaseData/GetModuleGroupConfig", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetModuleVisibleConfig(params, config) {
    return this.requestApi("BaseData/GetModuleVisibleConfig", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetDoctorDictionary(params, config) {
    return this.requestApi("BaseData/GetDoctorDictionary", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCSyncSync(params, config) {
    return this.requestApi("ChinaCPCSync/Sync", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCSyncSyncLastUploadFail(params, config) {
    return this.requestApi("ChinaCPCSync/SyncLastUploadFail", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserAddChinaCPCUser(params, config) {
    return this.requestApi("ChinaCPCUser/AddChinaCPCUser", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserDeleteChinaCPCUser(params, config) {
    return this.requestApi("ChinaCPCUser/DeleteChinaCPCUser", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserUpdateChinaCPCUser(params, config) {
    return this.requestApi("ChinaCPCUser/UpdateChinaCPCUser", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserGetChinaCPCUser(params, config) {
    return this.requestApi("ChinaCPCUser/GetChinaCPCUser", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserGetChinaCPCUserPager(params, config) {
    return this.requestApi("ChinaCPCUser/GetChinaCPCUserPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserGetChinaCPCRoles(params, config) {
    return this.requestApi("ChinaCPCUser/GetChinaCPCRoles", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserGetSystemUsers(params, config) {
    return this.requestApi("ChinaCPCUser/GetSystemUsers", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCUserGetChinaCPCUserBySystemId(params, config) {
    return this.requestApi("ChinaCPCUser/GetChinaCPCUserBySystemId", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataImportImport(params, config) {
    return this.requestApi("DataImport/Import", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataImportGetLatestDataImportList(params, config) {
    return this.requestApi("DataImport/GetLatestDataImportList", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataImportDoConvert(params, config) {
    return this.requestApi("DataImport/DoConvert", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataImportDoImport(params, config) {
    return this.requestApi("DataImport/DoImport", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  dataImportGetConvertId(params, config) {
    return this.requestApi("DataImport/GetConvertId", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  frontDisplayGetCRFDisplayConfig(params, config) {
    return this.requestApi("FrontDisplay/GetCRFDisplayConfig", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataAddGriddingHospital(params, config) {
    return this.requestApi("ChinaCPCBaseData/AddGriddingHospital", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataDeleteGriddingHospitals(params, config) {
    return this.requestApi("ChinaCPCBaseData/DeleteGriddingHospitals", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataUpdateGriddingHospital(params, config) {
    return this.requestApi("ChinaCPCBaseData/UpdateGriddingHospital", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetGriddingHospitalPager(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetGriddingHospitalPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetEnableGriddingHospitalPager(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetEnableGriddingHospitalPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetGriddingHospital(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetGriddingHospital", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataAddDepartment(params, config) {
    return this.requestApi("ChinaCPCBaseData/AddDepartment", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataDeleteDepartments(params, config) {
    return this.requestApi("ChinaCPCBaseData/DeleteDepartments", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataUpdateDepartment(params, config) {
    return this.requestApi("ChinaCPCBaseData/UpdateDepartment", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetDepartmentPager(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetDepartmentPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetEnableDepartmentPager(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetEnableDepartmentPager", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  chinaCPCBaseDataGetDepartment(params, config) {
    return this.requestApi("ChinaCPCBaseData/GetDepartment", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountLogin(params, config) {
    return this.requestApi("UserAccount/Login", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountLogout(params, config) {
    return this.requestApi("UserAccount/Logout", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetMenu(params, config) {
    return this.requestApi("UserAccount/GetMenu", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountUpdatePassword(params, config) {
    return this.requestApi("UserAccount/UpdatePassword", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetUserAccounts(params, config) {
    return this.requestApi("UserAccount/GetUserAccounts", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountDeleteUserAccount(params, config) {
    return this.requestApi("UserAccount/DeleteUserAccount", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetUserAccount(params, config) {
    return this.requestApi("UserAccount/GetUserAccount", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetDepartments(params, config) {
    return this.requestApi("UserAccount/GetDepartments", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetRoles(params, config) {
    return this.requestApi("UserAccount/GetRoles", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountSaveUserAccount(params, config) {
    return this.requestApi("UserAccount/SaveUserAccount", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountResetPassword(params, config) {
    return this.requestApi("UserAccount/ResetPassword", params, API_KEY, config);
  }

  /**
   * 
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAccountGetTokenByOpenId(params, config) {
    return this.requestApi("UserAccount/GetTokenByOpenId", params, API_KEY, config);
  }


}