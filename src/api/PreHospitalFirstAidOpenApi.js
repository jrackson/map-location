
import BaseApi from '../common/BaseApi';

// 基础映射配置
const API_KEY = "preHospitalFirstAidOpenApi";

/**
 * PreHospitalFirstAidOpenApi
 */
export default class PreHospitalFirstAidOpenApi extends BaseApi {


  /**
   * 获取多媒体信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  attachmentGetMp4(params, config) {
    return this.requestApi("Attachment/GetMp4", params, API_KEY, config);
  }

  /**
   * 根据指定的字典分类获取字典
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetDictionary(params, config) {
    return this.requestApi("BaseData/GetDictionary", params, API_KEY, config);
  }

  /**
   * 获取多媒体信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetMultiMedia(params, config) {
    return this.requestApi("BaseData/GetMultiMedia", params, API_KEY, config);
  }

  /**
   * 获取Grace评分项
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetGraceScoreDictionary(params, config) {
    return this.requestApi("BaseData/GetGraceScoreDictionary", params, API_KEY, config);
  }

  /**
   * 获取登录页显示信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetLoginDisplay(params, config) {
    return this.requestApi("BaseData/GetLoginDisplay", params, API_KEY, config);
  }

  /**
   * 获取微信签名
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  baseDataGetWeChatSignature(params, config) {
    return this.requestApi("BaseData/GetWeChatSignature", params, API_KEY, config);
  }

  /**
   * 创建急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientCreateEmergencyPatient(params, config) {
    return this.requestApi("EmergencyPatient/CreateEmergencyPatient", params, API_KEY, config);
  }

  /**
   * 获取急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEmergencyPatientUpdateDetail(params, config) {
    return this.requestApi("EmergencyPatient/GetEmergencyPatientUpdateDetail", params, API_KEY, config);
  }

  /**
   * 修改急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientUpdateEmergencyPatient(params, config) {
    return this.requestApi("EmergencyPatient/UpdateEmergencyPatient", params, API_KEY, config);
  }

  /**
   * 删除急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientDeleteEmergencyPatient(params, config) {
    return this.requestApi("EmergencyPatient/DeleteEmergencyPatient", params, API_KEY, config);
  }

  /**
   * 申请端获取急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEmergencyPatientsByApplySide(params, config) {
    return this.requestApi("EmergencyPatient/GetEmergencyPatientsByApplySide", params, API_KEY, config);
  }

  /**
   * 受理端获取急救患者
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEmergencyPatientsByAcceptSide(params, config) {
    return this.requestApi("EmergencyPatient/GetEmergencyPatientsByAcceptSide", params, API_KEY, config);
  }

  /**
   * 获取Grace评分
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetGraceScore(params, config) {
    return this.requestApi("EmergencyPatient/GetGraceScore", params, API_KEY, config);
  }

  /**
   * 更新Grace评分
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientPutGraceScore(params, config) {
    return this.requestApi("EmergencyPatient/PutGraceScore", params, API_KEY, config);
  }

  /**
   * 获取生命体征
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetVitalSigns(params, config) {
    return this.requestApi("EmergencyPatient/GetVitalSigns", params, API_KEY, config);
  }

  /**
   * 更新生命体征
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientPutVitalSigns(params, config) {
    return this.requestApi("EmergencyPatient/PutVitalSigns", params, API_KEY, config);
  }

  /**
   * 发送照片和视频
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientPutMultiMedia(params, config) {
    return this.requestApi("EmergencyPatient/PutMultiMedia", params, API_KEY, config);
  }

  /**
   * 发送文本消息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientPutText(params, config) {
    return this.requestApi("EmergencyPatient/PutText", params, API_KEY, config);
  }

  /**
   * 受理病人
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientAcceptRecord(params, config) {
    return this.requestApi("EmergencyPatient/AcceptRecord", params, API_KEY, config);
  }

  /**
   * 结束受理
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientEndRecord(params, config) {
    return this.requestApi("EmergencyPatient/EndRecord", params, API_KEY, config);
  }

  /**
   * 启动胸痛
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientStartUpChestPain(params, config) {
    return this.requestApi("EmergencyPatient/StartUpChestPain", params, API_KEY, config);
  }

  /**
   * 获取急救信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEmergencyDetail(params, config) {
    return this.requestApi("EmergencyPatient/GetEmergencyDetail", params, API_KEY, config);
  }

  /**
   * 获取急救信息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEmergencyFullDetail(params, config) {
    return this.requestApi("EmergencyPatient/GetEmergencyFullDetail", params, API_KEY, config);
  }

  /**
   * 获取Grace评分项
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetGraceSocreByItem(params, config) {
    return this.requestApi("EmergencyPatient/GetGraceSocreByItem", params, API_KEY, config);
  }

  /**
   * 启动卒中
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientStartUpStroke(params, config) {
    return this.requestApi("EmergencyPatient/StartUpStroke", params, API_KEY, config);
  }

  /**
   * 获取评分【FAST/NIHSS】项
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEvaluationScoreItem(params, config) {
    return this.requestApi("EmergencyPatient/GetEvaluationScoreItem", params, API_KEY, config);
  }

  /**
   * 获取评分【FAST/NIHSS】
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetEvaluationScore(params, config) {
    return this.requestApi("EmergencyPatient/GetEvaluationScore", params, API_KEY, config);
  }

  /**
   * 提交评分【FAST/NIHSS】
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientEvaluationScoreSubmit(params, config) {
    return this.requestApi("EmergencyPatient/EvaluationScoreSubmit", params, API_KEY, config);
  }

  /**
   * 高德地图距离和用时更新
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientUpdatePositionByAMap(params, config) {
    return this.requestApi("EmergencyPatient/UpdatePositionByAMap", params, API_KEY, config);
  }

  /**
   * 获取高德地图自动定位时间间隔
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  emergencyPatientGetAMapTimeInterval(params, config) {
    return this.requestApi("EmergencyPatient/GetAMapTimeInterval", params, API_KEY, config);
  }

  /**
   * 获取未读消息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  messageGetNewMessage(params, config) {
    return this.requestApi("Message/GetNewMessage", params, API_KEY, config);
  }

  /**
   * 获取消息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  messageGetMessage(params, config) {
    return this.requestApi("Message/GetMessage", params, API_KEY, config);
  }

  /**
   * 获取所有消息
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  messageGetAllMessage(params, config) {
    return this.requestApi("Message/GetAllMessage", params, API_KEY, config);
  }

  /**
   * 设置已读最新消息时间
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  messageSetLastMessageTime(params, config) {
    return this.requestApi("Message/SetLastMessageTime", params, API_KEY, config);
  }

  /**
   * 获取Grace评分项
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  unitTestingHelperGetGraceSocreTest(params, config) {
    return this.requestApi("UnitTestingHelper/GetGraceSocreTest", params, API_KEY, config);
  }

  /**
   * 获取受理端登录用户列表
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userGetAcceptLoginUsers(params, config) {
    return this.requestApi("User/GetAcceptLoginUsers", params, API_KEY, config);
  }

  /**
   * 申请端通过微信openid登录
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userApplySideLoginByOpenId(params, config) {
    return this.requestApi("User/ApplySideLoginByOpenId", params, API_KEY, config);
  }

  /**
   * 申请端通过微信openid登录
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAcceptSideLoginByOpenId(params, config) {
    return this.requestApi("User/AcceptSideLoginByOpenId", params, API_KEY, config);
  }

  /**
   * 申请端账号密码登录
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userApplySideLogin(params, config) {
    return this.requestApi("User/ApplySideLogin", params, API_KEY, config);
  }

  /**
   * 受理端账号密码登录
   * @param {Object} params 参数
   * @param {Object} config 配置
   */
  userAcceptSideLogin(params, config) {
    return this.requestApi("User/AcceptSideLogin", params, API_KEY, config);
  }


}