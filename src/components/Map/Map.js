import React, { Component } from 'react';
import {
  Icon,
  NavBar,
} from 'antd-mobile';
import './style.scss';
// import PreHospitalFirstAidOpenApi from '../../api/PreHospitalFirstAidOpenApi';
import { initMap, addGeolocation, geolocate, onGeolocationComplete } from '../../utils/aMap'

class Map extends Component {
  state = {
    DestinationName: '加载中...',
    Distance: '--',
    DistanceAndDurationLastUpdateTime: '--',
    Duration: '--',
  }
  //取消返回
  onCancel = () => {
    this.props.history.goBack();
  }

  onComplete = async data => {
    const { id } = this.props.match.params;
    const { position } = data
    const { lng: Longitude, lat: Latitude } = position
    // const { Data: res } = await this.preHospitalFirstAidOpenApi.emergencyPatientUpdatePositionByAMap({ PatientId: id, Longitude, Latitude })
    // this.setState({
    //   ...res
    // })
  }

  onError = data => {
    console.log('error', data)
  }

  async componentDidMount() {
    // this.preHospitalFirstAidOpenApi = new PreHospitalFirstAidOpenApi();
    const map = initMap('map')
    const geo = await addGeolocation({ map })
    // const { Data: interval } = await this.preHospitalFirstAidOpenApi.emergencyPatientGetAMapTimeInterval()
    // 取消定时定位
    // this.timer = setInterval(() => {
    //   geolocate(geo)
    // }, interval * 1000)
    geolocate(geo)
    onGeolocationComplete(geo, this.onComplete, this.onError)
  } 

  componentWillUnmount() {
    // clearInterval(this.timer)
  }

  render() {
    const { DestinationName, Distance, DistanceAndDurationLastUpdateTime, Duration } = this.state;
    return (
      <div className='phc-distance-use-time-container'>
        <NavBar
          mode='dark'
          className='phc-distance-use-time-navbar'
          icon={<Icon type='left' />}
          onLeftClick={() => {
            this.onCancel()
          }}
        >预计距离和用时</NavBar>
        <div className="cards">
          <div>目的地：{DestinationName}</div>
          <div>距离：{Distance}km，预计时间：{Duration}min</div>
          <div>更新时间：{DistanceAndDurationLastUpdateTime}</div>
        </div>
        <div className='map-container' id="map">
        </div>
      </div>
    )
  }
}

export default Map;
