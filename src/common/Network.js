import { Toast } from 'antd-mobile';

/**
 * 网状态变化工具
 * @author zhangYun
 * @copyright nalong
 * @date 2018-10-11
 */

/**
 * 网络状况改变
 */
export const networkOnChange = (on, off) => {
    const onLine = () => {
      if (on && typeof on === 'function') {
        on({status: true, msg: '在线'});
      }
    }
    const offLine = () => {
      Toast.offline('世界上最遥远的距离就是没网...');
      if (off && typeof off === 'function') {
        off({status: false, msg: '离线'});
      }
    }

    if (window.addEventListener) {
      window.addEventListener("online", onLine, true);
      window.addEventListener("offline", offLine, true);
    }
    else if (window.attachEvent) {
      window.attachEvent("ononline", onLine);
      window.attachEvent("onoffline", offLine);
    }
    else {
      window.ononline = onLine;
      window.onoffline = offLine;
    }
}

/**
 * 获取网路状态
 * @return {Boolean} true 在线 false 离线
 */
export const getNetworkStatus = () => {
  return navigator.onLine;
}
