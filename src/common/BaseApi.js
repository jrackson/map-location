import {Toast} from 'antd-mobile';
import React from 'react';
import {RESULT, STORGE_KEY} from '../const/Const';
import Common from '../common/Common';
import axios from 'axios';

export default class BaseApi {


  /**
   * 请求api
   * @param {String} url 映射
   * @param {Object} params 参数
   * @param {String} apiKey api
   * @param {Object} config 自定义配置
   */
  requestApi(url, params, apiKey, config) {
    return this.request(Object.assign({
      url: url,
      baseURL: this.getBaseApi(apiKey),
      params: params
    }, config));
  }

  /**
   * 获取基础路径
   * @param {String} apiKey API的键
   */
  getBaseApi(apiKey) {
    if (!window.getApiConfig) {
      console.error('没有检测到api配置，请确认config.js在根目录下!，并且被引用');
      return '';
    }
    return window.getApiConfig(process.env.NODE_ENV)[apiKey];
  }

  // Request Config
  // {
  //   // `url` is the server URL that will be used for the request
  //   url: '/user',
  //
  //   // `method` is the request method to be used when making the request
  //   method: 'get', // default
  //
  //   // `baseURL` will be prepended to `url` unless `url` is absolute.
  //   // It can be convenient to set `baseURL` for an instance of axios to pass relative URLs
  //   // to methods of that instance.
  //   baseURL: 'https://some-domain.com/api/',
  //
  //   // `transformRequest` allows changes to the request data before it is sent to the server
  //   // This is only applicable for request methods 'PUT', 'POST', and 'PATCH'
  //   // The last function in the array must return a string or an instance of Buffer, ArrayBuffer,
  //   // FormData or Stream
  //   // You may modify the headers object.
  //   transformRequest: [function (data, headers) {
  //     // Do whatever you want to transform the data
  //
  //     return data;
  //   }],
  //
  //   // `transformResponse` allows changes to the response data to be made before
  //   // it is passed to then/catch
  //   transformResponse: [function (data) {
  //     // Do whatever you want to transform the data
  //
  //     return data;
  //   }],
  //
  //   // `headers` are custom headers to be sent
  //   headers: {'X-Requested-With': 'XMLHttpRequest'},
  //
  //   // `params` are the URL parameters to be sent with the request
  //   // Must be a plain object or a URLSearchParams object
  //   params: {
  //     ID: 12345
  //   },
  //
  //   // `paramsSerializer` is an optional function in charge of serializing `params`
  //   // (e.g. https://www.npmjs.com/package/qs, http://api.jquery.com/jquery.param/)
  //   paramsSerializer: function(params) {
  //     return Qs.stringify(params, {arrayFormat: 'brackets'})
  //   },
  //
  //   // `data` is the data to be sent as the request body
  //   // Only applicable for request methods 'PUT', 'POST', and 'PATCH'
  //   // When no `transformRequest` is set, must be of one of the following types:
  //   // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
  //   // - Browser only: FormData, File, Blob
  //   // - Node only: Stream, Buffer
  //   data: {
  //     firstName: 'Fred'
  //   },
  //
  //   // `timeout` specifies the number of milliseconds before the request times out.
  //   // If the request takes longer than `timeout`, the request will be aborted.
  //   timeout: 1000,
  //
  //   // `withCredentials` indicates whether or not cross-site Access-Control requests
  //   // should be made using credentials
  //   withCredentials: false, // default
  //
  //   // `adapter` allows custom handling of requests which makes testing easier.
  //   // Return a promise and supply a valid response (see lib/adapters/README.md).
  //   adapter: function (config) {
  //     /* ... */
  //   },
  //
  //   // `auth` indicates that HTTP Basic auth should be used, and supplies credentials.
  //   // This will set an `Authorization` header, overwriting any existing
  //   // `Authorization` custom headers you have set using `headers`.
  //   auth: {
  //     username: 'janedoe',
  //     password: 's00pers3cret'
  //   },
  //
  //   // `responseType` indicates the type of data that the server will respond with
  //   // options are 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
  //   responseType: 'json', // default
  //
  //   // `xsrfCookieName` is the name of the cookie to use as a value for xsrf token
  //   xsrfCookieName: 'XSRF-TOKEN', // default
  //
  //   // `xsrfHeaderName` is the name of the http header that carries the xsrf token value
  //   xsrfHeaderName: 'X-XSRF-TOKEN', // default
  //
  //   // `onUploadProgress` allows handling of progress events for uploads
  //   onUploadProgress: function (progressEvent) {
  //     // Do whatever you want with the native progress event
  //   },
  //
  //   // `onDownloadProgress` allows handling of progress events for downloads
  //   onDownloadProgress: function (progressEvent) {
  //     // Do whatever you want with the native progress event
  //   },
  //
  //   // `maxContentLength` defines the max size of the http response content allowed
  //   maxContentLength: 2000,
  //
  //   // `validateStatus` defines whether to resolve or reject the promise for a given
  //   // HTTP response status code. If `validateStatus` returns `true` (or is set to `null`
  //   // or `undefined`), the promise will be resolved; otherwise, the promise will be
  //   // rejected.
  //   validateStatus: function (status) {
  //     return status >= 200 && status < 300; // default
  //   },
  //
  //   // `maxRedirects` defines the maximum number of redirects to follow in node.js.
  //   // If set to 0, no redirects will be followed.
  //   maxRedirects: 5, // default
  //
  //   // `socketPath` defines a UNIX Socket to be used in node.js.
  //   // e.g. '/var/run/docker.sock' to send requests to the docker daemon.
  //   // Only either `socketPath` or `proxy` can be specified.
  //   // If both are specified, `socketPath` is used.
  //   socketPath: null, // default
  //
  //   // `httpAgent` and `httpsAgent` define a custom agent to be used when performing http
  //   // and https requests, respectively, in node.js. This allows options to be added like
  //   // `keepAlive` that are not enabled by default.
  //   httpAgent: new http.Agent({ keepAlive: true }),
  //   httpsAgent: new https.Agent({ keepAlive: true }),
  //
  //   // 'proxy' defines the hostname and port of the proxy server
  //   // Use `false` to disable proxies, ignoring environment variables.
  //   // `auth` indicates that HTTP Basic auth should be used to connect to the proxy, and
  //   // supplies credentials.
  //   // This will set an `Proxy-Authorization` header, overwriting any existing
  //   // `Proxy-Authorization` custom headers you have set using `headers`.
  //   proxy: {
  //     host: '127.0.0.1',
  //     port: 9000,
  //     auth: {
  //       username: 'mikeymike',
  //       password: 'rapunz3l'
  //     }
  //   },
  //
  //   // `cancelToken` specifies a cancel token that can be used to cancel the request
  //   // (see Cancellation section below for details)
  //   cancelToken: new CancelToken(function (cancel) {
  //   })
  // }
  /**
   * 配置
   * @param {Object} config
   */
  request(config) {
    // config['headers'] = Object.assign((config['headers'] || {}), {
    //   'Token': Common.getToken() || ''
    // });

    if (Common.getToken()) {
      config['url'] = config['url'] + '?token=' + Common.getToken() || '';
    }

    if (!config['method']) {
      config['method'] = 'post';
    }

    // config['timeout']= 5000;
    config['timeout']= null;

    if (['POST', 'PUT'].indexOf(config['method'].toLocaleUpperCase()) !== -1) {
      config['data'] = config['params'];
      config['params'] = {};
      if (config['isFile']) { // 文件上传
        const formData = new FormData();
        Object.keys(config['data']).forEach(key => {
          formData.append(key, config['data'][key]);
        })
        config['data'] = formData;
        config['headers'] = {"Content-Type": "multipart/form-data"};
        config['timeout']= null; // 上传文件不设置超时
      }
    }

    return new Promise((resolve, reject)=> {
      axios.request(config).then((response) => {

        // Response Schema
        // {
        //   // `data` is the response that was provided by the server
        //   data: {},
        //
        //   // `status` is the HTTP status code from the server response
        //   status: 200,
        //
        //   // `statusText` is the HTTP status message from the server response
        //   statusText: 'OK',
        //
        //   // `headers` the headers that the server responded with
        //   // All header names are lower cased
        //   headers: {},
        //
        //   // `config` is the config that was provided to `axios` for the request
        //   config: {},
        //
        //   // `request` is the request that generated this response
        //   // It is the last ClientRequest instance in node.js (in redirects)
        //   // and an XMLHttpRequest instance the browser
        //   request: {}
        // }
        const {status, data} = response;
        if (status !== 200) { // 服务器错误
          Toast.offline('服务器错误！');
          reject(status);
          return;
        }

        const resCode = data['Code'], resMsg = data['Msg'], resData = data['Data'];
        if (RESULT.SUCCESS === resCode) {
          resolve(data);
          return;
        }

        if ([RESULT.TOKEN_INVALID, RESULT.TOKEN_MISSING].indexOf(resCode) !== -1 ) {
          Toast.info('token失效！', 2, null, false);
          reject('token失效！');
          Common.clearSessionStore();

          // 重新登录
          let isServer = true;
          if (window.location.href.indexOf('/prehospital/client') !== -1) {
            isServer = false;
          }

          let loginRedirectUrl = window.location.origin + '/prehospital/' + (isServer ? 'server' : 'client') + '/login';

          if (Common.isWeChatBrower()) { // 微信浏览器自动授权登录
            const url = Common.getSessionStore(Common.getSystemKey(STORGE_KEY.REDIRECT_OATH_URL));
            if (url && url !== 'undefined') {
              loginRedirectUrl = url;
            }
            else { // 重新授权
              loginRedirectUrl = window.location.origin + '/PreHospitalFirstAidOpenAPI/Home/' + (isServer ? 'Server' : 'Client') + 'LoginRedirect';
            }
          }

          window.location.href = loginRedirectUrl;
          return;
        }

        let msg = resMsg;

        // 错误信息放在data里 key
        if (RESULT.MSG_DATA_CODE === resCode) {
          // 获取data中的数据作为消息显示
          // Data: {
          //     FullName: ['sss','sss']
          // }
          msg = Object.keys(resData).map((key,index) =>
            <div key={index}>
              <strong>{key}:</strong>
              <span>
                {
                  resData[key].map((val, i) =>
                    <span key={i}> {i === 0 ? '' : <br/>} {val}</span>
                  )
                }
              </span>
              <br/>
            </div>
          );
        }

        Toast.offline(msg);
        reject(msg);

      }).catch(err => {
        if (err.code === 'ECONNABORTED') {
          Toast.offline('网络超时！' + err.message);
        } else {
          Toast.offline(err.message);
        }
        reject(err);
      });
    });
  }
}

