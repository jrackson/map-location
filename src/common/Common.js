import {STORGE_KEY} from '../const/Const';
import moment from 'moment';

class Common {

  /**
   * 获取token
   */
  getToken() {
    return this.getSessionStore(this.getSystemKey(STORGE_KEY.TOKEN)) || '';
  }

  /**
   * 设置token
   * @param {String} token token
   */
  setToken(token) {
    this.setSessionStore(this.getSystemKey(STORGE_KEY.TOKEN), token);
  }

  /**
   * 从sessionStorage取值
   * @param {String} key 键
   */
  getSessionStore(key) {
    return this.getStore(sessionStorage, key);
  }

  /**
   * 向sessionStorage设置值
   * @param {String} key 键
   * @param {Object} value 值
   */
  setSessionStore(key, value) {
    this.setStore(sessionStorage, key, value);
  }

  /**
   * 清除sessionStorage
   */
  clearSessionStore() {
    this.clearStore(sessionStorage);
  }

  /**
   * 从localStorage取值
   * @param {String} key 键
   */
  getLocalStore(key) {
    return this.getStore(localStorage, key);
  }

  /**
   * 向localStorage设置值
   * @param {String} key 键
   * @param {Object} value 值
   */
  setLocalStore(key, value) {
    this.setStore(localStorage, key, value);
  }

  /**
   * 清除localStorage
   */
  clearLocalStore() {
    this.clearStore(localStorage);
  }

  /**
   * 从仓库取值
   * @param {Object} store 仓库
   * @param {String} key 键
   */
  getStore(store, key) {
    try {
      return JSON.parse(store.getItem(key));
    } catch (error) {
      return '';
    }
  }

  /**
   * 向仓库设置值
   * @param {Object} store 仓库
   * @param {String} key 键
   * @param {Object} value 值
   */
  setStore(store, key, value) {
    store.setItem(key, JSON.stringify(value));
  }

  /**
   * 清除仓库数据
   * @param {Object} store 仓库
   */
  clearStore(store) {
    store.clear();
  }

  /**
   * 获取key，分子系统
   */
  getSystemKey = (key) => {
    // 申请端
    if (window.location.href.indexOf('/prehospital/client') !== -1) {
      return key + 'Client';
    }
    return key + 'Server';
  }

  /**
   * 获取人类可读的时间差
   * 如果分钟数超过60分钟，自动计算分钟、小时、天、周、月、年、
   * 计算标准 60min=1hour、24hour=1day、30day=1month、12month=1year、 100year=1century
   * @param {number} minute 分钟数
   * @returns {string} 易读化时间显示
   * @author zhangYun
   * @date 2018-06-25
   */
  getReadabilityTime = (minute) => {
    let min = null;
    if (minute && !isNaN(minute)) {
      min = minute * 1;
    }
    if (min) {
      if (min < 60) {
        return min + '分钟';
      }
      if (min >= 60 && min < 60 * 24) {
        return Math.round(min / 60) + '小时';
      }
      if (min >= 60 * 24 && min < 60 * 24 * 7) {
        return Math.round(min / (60 * 24)) + '天';
      }
      if (min >= 60 * 24 * 7 && min < 60 * 24 * 30) {
        return Math.round(min / (60 * 24 * 7)) + '周';
      }
      if (min >= 60 * 24 * 30 && min < 60 * 24 * 30 * 12) {
        return Math.round(min / (60 * 24 * 30)) + '月';
      }
      if (min >= 60 * 24 * 30 * 12 && min < 60 * 24 * 30 * 12 * 100) {
        return Math.round(min / (60 * 24 * 30 * 12)) + '年';
      }
      if (min >= 60 * 24 * 30 * 12 * 100) {
        return Math.round(min / (60 * 24 * 30 * 12 * 100)) + '世纪';
      }
    }
    return '--';
  };

  /**
   * <p>日期解析</p>
   * @param {String} dateStr 待解析的日期字符串
   * @param {String} format 格式
   * @return {Date} 日期对象
   */
  parseDate(dateStr, format) {
    const date = new Date();
    const rex = new RegExp('yyyy|MM|dd|HH|mm|ss|SSS', 'g');
    let res = null;
    while ((res = rex.exec(format)) !== null) {
      switch (res[0]) {
        case 'yyyy':
          date.setFullYear(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 4)));
          break;
        case 'MM':
          date.setMonth(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 2)) - 1);
          break;
        case 'dd':
          date.setDate(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 2)));
          break;
        case 'HH':
          date.setHours(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 2)));
          break;
        case 'mm':
          date.setMinutes(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 2)));
          break;
        case 'ss':
          date.setSeconds(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 2)));
          break;
        case 'SSS':
          date.setMilliseconds(Number.parseFloat(dateStr.substring(res['index'], res['index'] + 3)));
          break;
      }
    }
    return date;
  }

  /**
   * <p>日期格式化</p>
   * @param {Date} date 待格式化的日期
   * @param {String} format 格式
   * @return {String} 格式化后的字符串
   */
  formatDate(date, format) {
    let _format = '' + format;
    const o = {
        'M+': date.getMonth() + 1, // month
        'd+': date.getDate(), // day
        'H+': date.getHours(), // hour
        'm+': date.getMinutes(), // minute
        's+': date.getSeconds(), // second
        'q+': Math.floor((date.getMonth() + 3) / 3), // quarter
        'S': date.getMilliseconds() // millisecond
    };

    if (/(y+)/.test(_format)) {
        _format = _format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (const k in o) {
        if (new RegExp('(' + k + ')').test(_format)) {
            _format = _format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
        }
    }
    if (_format === 'null') {
        _format = '';
    }
    return _format;
  }

  /**
   * 微信聊天消息时间显示说明
   * 1、当天的消息，以每5分钟为一个跨度的显示时间；
   * 2、消息超过1天、小于1周，显示星期+收发消息的时间；
   * 3、消息大于1周，显示手机收发时间的日期。
   * @param {Array<String, Object>} dateListMap
   */
  getWechatMessageTime(dateListMap) {
    const obj = {};
    const now = new Date();
    const getValue = (date, nowDay) => {
      // 1、当天的消息
      if (date.getTime() >= nowDay.getTime()) {
        return this.getTodayDate(date);
      }
      // 2、消息在昨天，显示昨天+收发消息的时间；
      else if (date.getTime() < nowDay.getTime()
        && date.getTime() > (nowDay.getTime() - 1 * 24 * 60 * 60 * 1000)) {
        return '昨天 ' + this.getTodayDate(date);
      }
      // 2、消息超过1天、小于1周，显示星期+收发消息的时间；
      else if (date.getTime() < nowDay.getTime()
        && date.getTime() > (nowDay.getTime() - nowDay.getDay() * 24 * 60 * 60 * 1000)) {
        return this.getWeekDate(date);
      }
      // 3、消息大于1周，显示手机收发时间的日期。
      else {
        return this.formatDate(date, 'yyyy年MM月dd日 ') + this.getTodayDate(date);
      }
    }
    // 今天凌晨
    const nowDay = this.parseDate(this.formatDate(now, 'yyyy-MM-dd') + ' 00:00:00', 'yyyy-MM-dd HH:mm:ss');
    dateListMap.forEach((dateMap, i) => {
      const dateString = dateMap['time'];
      const date = this.parseDate(dateString, 'yyyy-MM-dd HH:mm:ss');
      if (i > 0 && dateListMap.length - 1 > i) {
        const preDate = this.parseDate(dateListMap[i - 1]['time'], 'yyyy-MM-dd HH:mm:ss');
        if (dateMap['isKey'] === true) { // 强制显示
          obj[i] = getValue(date, nowDay);
        }
        // 以每5分钟为一个跨度的显示时间；
        else if (date.getTime() - preDate.getTime() >= 5 * 60 * 1000 ) {
          obj[i] = getValue(date, nowDay);
        }
        else { // >5min
          obj[i] = '';
        }
      }
      else { // 最后一个或第一个
        obj[i] = getValue(date, nowDay);
      }
    });
    return obj;
  }

  /**
   * 获取可读时间，包含上下午
   * @param {Date} date
   */
  getTodayDate(date) {
    const min = this.getDigit(date.getMinutes(), 2);
    if (date.getHours() > 12) {
      return '下午' + this.getDigit((date.getHours() - 12), 2) + ':' + min;
    }
    return '上午' + this.getDigit(date.getHours(), 2) + ':' + min;
  }

  /**
   * 获取可读时间，包含星期上下午
   * @param {Date} date
   */
  getWeekDate(date) {
    return '周' + ['日', '一', '二', '三', '四', '五', '六'][date.getDay()]
      + ' ' + this.getTodayDate(date);
  }

  /**
   * 前补0
   * @param {Number|String} num 数字
   * @param {Number|String} len 长度
   */
  getDigit(num, len) {
    const arr = [];
    const numArr = (num + '').split('').reverse();
    for (let i = 0; i < len; i++) {
      if (i < numArr.length) {
        arr.push(numArr[i]);
      }
      else {
        arr.push('0');
      }
    }
    return arr.reverse().join('');
  }

  /**
   * 是否微信内嵌浏览器
   */
  isWeChatBrower() {
    return window.navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1;
  }

  /**
   * 是否ios终端
   */
  isIos() {
    return !!window.navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
  }

}

export default new Common();