import React, {Component} from 'react';
import {List, InputItem, DatePicker, Switch} from 'antd-mobile';
import SelectItem from '../../plugins/SelectItem/SelectItem';
import StaffItem from '../../plugins/StaffItem/StaffItem';
import PropTypes from 'prop-types';
// 验证类型枚举，必填
const REQUIRE_TYPE = 4;

const CUSTOM_TYPE = {
  ACSAntiplateletNurse: 'staff-mulit',
}

/**
 * 动态组件
 */
class DynamicField extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired, // 字段的key
    name: PropTypes.string, // 字段的名称
    form: PropTypes.object.isRequired, // form
    fieldMap: PropTypes.object.isRequired, // 字段的名称
    fieldProps: PropTypes.object, // 字段配置属性
  }

  constructor(props) {
    super(props);
    this.state = {
      field: null, // 属性
    }
  }

  componentDidMount() {
    const {id, name, fieldMap} = this.props;
    this.init(id, name, fieldMap);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps.id, nextProps.name, nextProps.fieldMap);
  }

  /**
   * 初始化
   * @param {String} id 字段主键
   * @param {String} name 字段名称
   * @param {Object} fieldMap 配置
   */
  init(id, name, fieldMap) {
    if (fieldMap && fieldMap[id]) {
      const field = fieldMap[id];
      if (Object.keys(CUSTOM_TYPE).indexOf(id) !== -1) {
        field['FieldType'] = CUSTOM_TYPE[id];
      }
      this.setState({
        field: field
      })
    }
    else {
      console.error(`动态表单属性：没有检测到属性${id}: ${name}, 请检查后端配置！`)
    }
  }

  /**
   * 获取是否为隐藏
   */
  getFieldIsHidden(field) {
    if (field && field.hasOwnProperty('IsHidden')) {
      return field['IsHidden'];
    }
    return false;
  }

  /**
   * 渲染组件
   */
  renderComponent(field, from, fieldProps) {

    if (!field) {
      return '';
    }

    const fieldType = field['FieldType'];
    const fieldName = field['FieldName'];
    let fieldLabel = field['FieldLabel'];
    const optionData = field['OptionData'];
    const requiredType = field['RequiredType'];
    const validateRules = field['ValidateRules'];
    const defaultValue = field['DefaultValue'];
    const maxLength = field['MaxLength'] * 1 || 50;
    const {getFieldProps} = from;

    // 注册到form
    const options = {initialValue: defaultValue};
    if (fieldType === 'checkbox') {
      options.valuePropName = 'checked';
      options.initialValue = defaultValue || defaultValue === '1';
    }

    // 验证规则
    const rules = [];
    if (requiredType === REQUIRE_TYPE) {
      fieldLabel = <span>{fieldLabel}<span style={{color: 'red'}}>*</span></span>;
      rules.push({
        'required': true,
        'message': '此项为必填'
      });
    }
    if (validateRules) {
      validateRules.forEach(rule => {
        rules.push({
          pattern: rule['RuleContent'],
          message: rule['ErrorMessage']
        });
      });
    }

    if (rules && rules.length > 0) {
      options.rules = rules;
    }

    const props = {
      ...getFieldProps(fieldName, options),
      ...fieldProps
    };

    let component = '';
    switch (fieldType) {
      case 'text':
        component = <InputItem
          className='phc-input-item'
          {...props}
          clear
          maxLength={maxLength}
          placeholder={'请输入' + field['FieldLabel']}
        >{fieldLabel}</InputItem>;
        break;
      case 'date':
        component = <DatePicker
          {...props}
          maxDate={new Date()}
          title={fieldLabel}
          mode="date">
          <List.Item arrow="horizontal">{fieldLabel}</List.Item>
        </DatePicker>;
        break;
      case 'datetime':
        component = <DatePicker
          {...props}
          maxDate={new Date()}
          title={fieldLabel}
          mode="datetime">
          <List.Item arrow="horizontal">{fieldLabel}</List.Item>
        </DatePicker>;
        break;
      case 'checkbox':
        component = <List.Item
          extra={<Switch {...props} />}
        >{fieldLabel}</List.Item>;
        break;
      case 'radioGroup':
        component = <SelectItem
          {...props}
          label={fieldLabel}
          data={optionData}
          multi={false}>
        </SelectItem>;
        break;
      case 'dropdown':
        component = <SelectItem
          {...props}
          label={fieldLabel}
          data={optionData}
          multi={false}>
        </SelectItem>;
        break;
      case 'select-mulit': // 自定义多选
        component = <SelectItem
          {...props}
          label={fieldLabel}
          data={optionData}
          multi>
        </SelectItem>;
        break;
      case 'staff-mulit': // 自定义人员输入多选
        component = <StaffItem
          {...props}
          id={fieldName}
          label={fieldLabel} multi maxLength={maxLength}>
        </StaffItem>;
        break;
      default:
        component = '';
        break;
    }
    return component;
  }

  /**
   * 获取属性错误
   * @param {*} form 表单
   * @param {*} id 属性名
   */
  getFieldError(form, id) {
    return form.getFieldError(id);
  }

  render() {
    const {id, form, fieldProps} = this.props;
    const {field} = this.state;
    const isHidden = this.getFieldIsHidden(field);
    const errorMsg = this.getFieldError(form, id);

    const style = {
      'color': 'red',
      'height': '35px',
      'lineHeight': '35px',
      'borderBottom': '1px solid #f0f0f0',
      'marginLeft': '15px',
    };

    return (
      <div style={{display: isHidden ? 'none' : 'block'}}>
        {this.renderComponent(field, form, fieldProps)}
        {errorMsg ? <div style={style}>{errorMsg}</div> : ''}
      </div>
    );
  }
}

export default DynamicField;
