import React, { Component } from 'react';
import {Icon} from 'antd-mobile';
import PropTypes from 'prop-types';
import './style.scss';

export default class ListOperate extends Component {

  static propTypes = {
    pageIndex: PropTypes.any, // 第几页
    total: PropTypes.any, // 总记录数
    loading: PropTypes.bool, // 正在加载
    select: PropTypes.func, // 查询
  }

  constructor(props) {
    super(props);
    const {loading, pageIndex, total} = this.props;
    this.state = {
      loading: loading,
      pageIndex: pageIndex,
      total: total
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      loading: nextProps.loading,
      pageIndex: nextProps.pageIndex,
      total: nextProps.total
    })
}

  render() {
    const {select} = this.props;
    const {loading, pageIndex, total} = this.state;
    return (
      <div className="list-operate-container">
        {
          loading ? <Icon type="loading"></Icon> :
          (
            pageIndex === total || total === 0 ?
            '无更多数据':
            <span className="click-more" onClick={()=> {
              select(pageIndex + 1);
            }}>点击加载更多</span>
          )
        }
      </div>
    );
  }

}
