import React, { Component } from 'react';
import {Icon} from 'antd-mobile';
import PropTypes from 'prop-types';
import './style.scss';

export default class Operate extends Component {

  static propTypes = {
    cancelTitle: PropTypes.string, // 取消标题
    submitTitle: PropTypes.string, // 提交标题
    onCancel: PropTypes.func, // 取消
    onSubmit: PropTypes.func, // 提交
    loading: PropTypes.bool, // 正在加载
  }

  constructor(props) {
    super(props);
    const {loading} = this.props;
    this.state = {
      loading: loading,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.loading !== this.props.loading) {
      this.setState({
        loading: nextProps.loading,
      })
    }
  }

  render() {
    const {onCancel, onSubmit, cancelTitle, submitTitle} = this.props;
    const {loading} = this.state;
    return (
      <div className="operate-container">
        <div className="button cancel" onClick={onCancel}>
          <span>{cancelTitle || '取消'}</span>
        </div>
        <div className="button confirm" onClick={onSubmit}>
          {
            loading ? <Icon className="loading" type="loading" size='sm' /> : ''
          }
          <span>{submitTitle || '提交'}</span>
        </div>
      </div>
    );
  }

}
