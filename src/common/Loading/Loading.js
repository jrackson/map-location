import React, { Component } from 'react';
import {Icon} from 'antd-mobile';
import './style.scss';

export default class Loading extends Component {

  constructor(props) {
    super(props);
    const {loading} = this.props;
    this.state = {
      loading: loading,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.loading !== this.props.loading) {
      this.setState({
        loading: nextProps.loading,
      })
    }
  }

  render() {
    const {children} = this.props;
    const {loading} = this.state;

    if (loading) {
      return (
        <div className="loading-container">
          <Icon className="loading-icon" type="loading" size='md' />
        </div>
      );
    }

    return (<div>{children}</div>);
  }

}
