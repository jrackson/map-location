import { Toast } from 'antd-mobile';
import { WEB_SOCKET } from '../const/Const';
import { networkOnChange, getNetworkStatus } from './Network';
import Common from './Common';
import { STORGE_KEY } from '../const/Const'


// WebSocket格式
// body:
// MsgId  记录Id
// PatientId 患者Id
// MsgType  消息类型(1:文本,2:图片,3:视频)
// MsgBusinessType  消息业务类型 0:普通消息, 1:创建急救患者, 2:更新患者资料 3:受理病人 4:结束受理 5:启动胸痛, 6:Grace评分, 7:生命体征
// Title 消息标题
// MsgContent 消息内容
// MsgTime 创建时间

// 链接状态
const SOCKET_STATUS = {
  WAIT: 0, // 0 - 表示连接尚未建立。
  CONNECT: 1, // 1 - 表示连接已建立，可以进行通信。
  COLSEING: 2, // 2 - 表示连接正在进行关闭。
  COLSE: 3, // 3 - 表示连接已经关闭或者连接不能打开。
}

// 服务器接受消息类型
const SERVER_MESSAGE_TYPE = {
  MESSAGE_RECEIPT: 'MESSAGE_RECEIPT', // 消息回执
  CLINET_HEARTBEAT: 'CLINET_HEARTBEAT', // 心跳
  P2P: 'P2P', // 点对点传输
  PUBLISH: 'PUBLISH', // 推送
  PUBLISHBYGROUP: 'PUBLISHBYGROUP', // 推送通过组
  CLINET_CONNECTED: 'CLINET_CONNECTED', // 链接成功
  HEARTBEAT_SUCCESS: 'HEARTBEAT_SUCCESS', // 心跳成功
}

const MAX_RECONNECT_TIMES = 3; // 最大重连次数

/**
 * socket
 * @author zhangYun
 * @copyright nalong
 * @date 2018-10-11
 */
export default class Socket {

  socket = null; // websocket
  daemonInterval = null; // 守护进程定时
  listeners = []; // 服务器消息监听器
  reconnectCount = 0; // 重连次数

  constructor() {
    this.openDaemon();
    // 网络监听
    this.networkListener();

    if (window.addEventListener) {
      window.addEventListener("unload", this.closeDaemon, true);
    }
    else if (window.attachEvent) {
      window.attachEvent("unload", this.closeDaemon);
    }
  }

  // 网络监听
  networkListener() {
    networkOnChange(data => {
      // 在线
      this.openDaemon();
    }, data => {
      // 离线
      this.closeDaemon();
    })
  }

  // 连接socket
  connect() {
    const token = Common.getSessionStore(Common.getSystemKey(STORGE_KEY.WS_TOKEN));
    if (!token) {
      return;
    }
    let isServer = true;
    if (window.location.href.indexOf('/prehospital/client') !== -1) {
      isServer = false;
    }
    const wsUrl = window.getApiConfig(process.env.NODE_ENV)['webSocket' + (isServer ? 'Server' : 'Client')] + '?token=' + token;
    this.socket = new WebSocket(wsUrl);
    this.socket.onmessage = (message) => {
      // 接收到消息
      const wsData = JSON.parse(message.data);
      if (wsData['needReceipt']) { // 需要回执
        this.sendMessage({
          type: SERVER_MESSAGE_TYPE.MESSAGE_RECEIPT,
          sourcemessage: message.data,
          receiptmessage: '消息回执',
        });
      }

      this.listeners.forEach(listener => {
        if (wsData.type === SERVER_MESSAGE_TYPE.P2P) {
          listener(JSON.parse(wsData.body), message);
        }
      });
    }
    this.socket.onerror = (error) => {
      console.log('连接错误');
      if (this.reconnectCount > MAX_RECONNECT_TIMES) {
        Toast.fail('通道连接失败!');
      }
      this.reconnectCount = this.reconnectCount + 1;
    }
    this.socket.close = () => {
      console.log('连接关闭');
    }
    this.socket.onopen = () => {
      console.log('连接建立');
      // 重置连接次数
      this.reconnectCount = 0;
    }
  }

  /**
   * 消息监听器
   */
  messageListener = (listener) => {
    if (listener && typeof listener === 'function') {
      this.listeners.push(listener);
    }
  }

  // 发送消息
  sendMessage = (msg) => {
    if (this.socket && this.socket.readyState === SOCKET_STATUS.CONNECT
      && getNetworkStatus()) { // 在线并且socket在线
      this.socket.send(msg);
    }
  }

  // 开启守护进程
  openDaemon() {
    const connect = () => {
      if (getNetworkStatus()) { // 在线
        // socket正常连接
        if (this.socket && this.socket.readyState === SOCKET_STATUS.CONNECT) {
          // 心跳包
          this.sendMessage(JSON.stringify({
            "type": SERVER_MESSAGE_TYPE.CLINET_HEARTBEAT,
            "domain": window.location.host,
            "user": Common.getSessionStore(Common.getSystemKey(STORGE_KEY.TOKEN)) || "",
            "resource": window.navigator.appVersion,
            "lasttimestamp": new Date().getTime()
          }));
        }
        else { // socket关闭
          this.connect();
        }
      }
    }
    // 重新链接
    connect();
    // 检测
    this.daemonInterval = setInterval(()=> {
      connect();
    }, WEB_SOCKET.HEART_BEAT_TIME);
  }

  // 关闭守护进程
  closeDaemon() {
    if (this.socket) {
      this.socket.close();
    }
    clearInterval(this.daemonInterval);
  }

}